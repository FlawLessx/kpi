// To parse this JSON data, do
//
//     final divisiModel = divisiModelFromJson(jsonString);

import 'dart:convert';

import 'package:kpi_system/core/datasources/database/db_utils.dart';

DivisiModel divisiModelFromJson(String str) =>
    DivisiModel.fromJson(json.decode(str));

String divisiModelToJson(DivisiModel data) => json.encode(data.toJson());

class DivisiModel {
  DivisiModel({
    this.id,
    required this.namaDivisi,
  });

  int? id;
  String namaDivisi;

  factory DivisiModel.fromJson(Map<dynamic, dynamic> json) => DivisiModel(
        id: json[columnId],
        namaDivisi: json["nama_divisi"],
      );

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{
      "nama_divisi": namaDivisi,
    };
    if (id != null) {
      map["id"] = id;
    }
    return map;
  }
}
