// To parse this JSON data, do
//
//     final nilaiKpiModel = nilaiKpiModelFromJson(jsonString);

import 'dart:convert';

import 'package:kpi_system/core/datasources/database/db_utils.dart';

NilaiKpiModel nilaiKpiModelFromJson(String str) =>
    NilaiKpiModel.fromJson(json.decode(str));

String nilaiKpiModelToJson(NilaiKpiModel data) => json.encode(data.toJson());

class NilaiKpiModel {
  NilaiKpiModel(
      {this.id,
      required this.idKategory,
      required this.kodeDivisi,
      required this.skorKpi,
      required this.idKaryawan,
      required this.bukti,
      required this.status,
      required this.lock,
      required this.tanggal,
      this.alasan});

  int? id;
  int kodeDivisi;
  List<int> idKategory;
  List<int> skorKpi;
  int idKaryawan;
  String bukti;
  String status;
  bool lock;
  String tanggal;
  String? alasan;

  factory NilaiKpiModel.fromJson(Map<dynamic, dynamic> json) => NilaiKpiModel(
        id: json[columnId],
        kodeDivisi: json[columnKodeDivisi],
        idKategory: utf8.encode(json["id_kategory"]),
        skorKpi: utf8.encode(json["skor_kpi"]),
        idKaryawan: json["id_karyawan"],
        bukti: json["bukti"],
        status: json["status"],
        lock: json["lock"] == 1,
        tanggal: json["tanggal"],
        alasan: json[columnAlasan],
      );

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{
      "id_kategory": utf8.decode(idKategory),
      columnKodeDivisi: kodeDivisi,
      "skor_kpi": utf8.decode(skorKpi),
      "id_karyawan": idKaryawan,
      "bukti": bukti,
      "status": status,
      "lock": lock == true ? 1 : 0,
      "tanggal": tanggal,
      columnAlasan: alasan,
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }
}
