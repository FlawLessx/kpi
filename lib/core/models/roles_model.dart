import 'dart:convert';

import 'package:kpi_system/core/datasources/database/db_utils.dart';

RolesModel rolesModelFromJson(String str) =>
    RolesModel.fromJson(json.decode(str));

String rolesModelToJson(RolesModel data) => json.encode(data.toJson());

class RolesModel {
  RolesModel({
    this.id,
    required this.namaRole,
  });

  int? id;
  String namaRole;

  factory RolesModel.fromJson(Map<dynamic, dynamic> json) => RolesModel(
        id: json[columnId],
        namaRole: json["nama_role"],
      );

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{
      "nama_role": namaRole,
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }
}
