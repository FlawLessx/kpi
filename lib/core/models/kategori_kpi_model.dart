// To parse this JSON data, do
//
//     final kategoriKpiModel = kategoriKpiModelFromJson(jsonString);

import 'dart:convert';

import 'package:kpi_system/core/datasources/database/db_utils.dart';

KategoriKpiModel kategoriKpiModelFromJson(String str) =>
    KategoriKpiModel.fromJson(json.decode(str));

String kategoriKpiModelToJson(KategoriKpiModel data) =>
    json.encode(data.toJson());

class KategoriKpiModel {
  KategoriKpiModel({
    this.id,
    required this.kodeDivisi,
    required this.kategoriKpi,
    this.totalKpi,
    required this.target,
  });

  int? id;
  int kodeDivisi;
  String kategoriKpi;
  int? totalKpi;
  int target;

  factory KategoriKpiModel.fromJson(Map<dynamic, dynamic> json) =>
      KategoriKpiModel(
        id: json[columnId],
        kodeDivisi: json["kode_divisi"],
        kategoriKpi: json["kategori_kpi"],
        totalKpi: json["total_kpi"],
        target: json["target"],
      );

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{
      "kode_divisi": kodeDivisi,
      "kategori_kpi": kategoriKpi,
      "total_kpi": totalKpi,
      "target": target,
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }
}
