// To parse this JSON data, do
//
//     final userModel = userModelFromJson(jsonString);

import 'dart:convert';

import 'package:kpi_system/core/datasources/database/db_utils.dart';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  UserModel(
      {this.id,
      required this.nama,
      required this.kodeDivisi,
      required this.email,
      required this.password,
      required this.noTelp,
      required this.rolesId,
      required this.jenisKelamin,
      required this.userName});

  int? id;
  final String nama;
  final int kodeDivisi;
  final String email;
  final String password;
  final String noTelp;
  final String userName;
  final int rolesId;
  final bool jenisKelamin;

  factory UserModel.fromJson(Map<dynamic, dynamic> json) => UserModel(
        id: json[columnId],
        nama: json["nama"],
        kodeDivisi: json["kode_divisi"],
        email: json["email"],
        password: json["password"],
        noTelp: json["no_telp"],
        rolesId: json["roles_id"],
        userName: json['username'],
        jenisKelamin: json["jenis_kelamin"] == 1,
      );

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{
      "nama": nama,
      "kode_divisi": kodeDivisi,
      "email": email,
      "password": password,
      "no_telp": noTelp,
      "roles_id": rolesId,
      'username': userName,
      "jenis_kelamin": jenisKelamin == true ? 1 : 0,
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }
}
