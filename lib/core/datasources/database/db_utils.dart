final String columnId = '_id';

// User Table
final String tableUser = 'user_table';
final String columnKodeDivisi = 'kode_divisi';
final String columnNama = 'nama';
final String columnEmail = 'email';
final String columnPassword = 'password';
final String columnUsername = 'username';
final String columnNoTelp = 'no_telp';
final String columnRoles = 'roles_id';
final String columnJenisKelamin = 'jenis_kelamin';

// Roles Table
final String tableRole = 'role_table';
final String columnNamaRole = 'nama_role';

// Divisi Table
final String tableDivisi = 'divisi_table';
final String columnNamaDivisi = 'nama_divisi';

// Kategori KPI Table
final String tableKategoriKPI = 'kategori_kpi_table';
final String columnKategori = 'kategori_kpi';
final String columnTotalKPI = 'total_kpi';
final String columnTarget = 'target';

// Nilai KPI Table
final String tableNilaiKPI = 'nilai_kpi_table';
final String columnIdKategory = 'id_kategory';
final String columnSkorKPI = 'skor_kpi';
final String columnIdKaryawan = 'id_karyawan';
final String columnBukti = 'bukti';
final String columnStatus = 'status';
final String columnLock = 'lock';
final String columnTanggal = 'tanggal';
final String columnAlasan = 'alasan';
