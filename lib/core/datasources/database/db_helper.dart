import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import '../../models/divisi_model.dart';
import '../../models/kategori_kpi_model.dart';
import '../../models/nilai_kpi_model.dart';
import '../../models/roles_model.dart';
import '../../models/user_model.dart';
import 'db_utils.dart';

class DBHelper {
  static final DBHelper _instance = new DBHelper.internal();
  factory DBHelper() => _instance;

  static Database? _db;

  Future<Database?> get db async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  initDb() async {
    var directory = await getApplicationDocumentsDirectory();
    String path = p.join(directory.toString(), "main.db");
    var theDb = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute('''
          create table $tableUser (
          $columnId integer primary key autoincrement,
          $columnKodeDivisi integer,
          $columnNama text,
          $columnEmail email,
          $columnPassword text,
          $columnUsername text,
          $columnNoTelp text,
          $columnRoles integer,
          $columnJenisKelamin integer)
          ''');

      await db.execute('''
          create table $tableRole (
          $columnId integer primary key autoincrement,
          $columnNamaRole text non null)
          ''');

      await db.execute('''
          create table $tableDivisi (
          $columnId integer primary key autoincrement,
          $columnNamaDivisi text non null)
          ''');

      await db.execute('''
          create table $tableKategoriKPI (
          $columnId integer primary key autoincrement,
          $columnKodeDivisi integer,
          $columnKategori text,
          $columnTotalKPI integer,
          $columnTarget integer)
          ''');

      await db.execute('''
          create table $tableNilaiKPI (
          $columnId integer primary key autoincrement,
          $columnIdKategory text,
          $columnKodeDivisi integer,
          $columnSkorKPI text,
          $columnIdKaryawan integer,
          $columnBukti text,
          $columnStatus text,
          $columnLock integer,
          $columnTanggal text,
          $columnAlasan text)
          ''');
    });
    return theDb;
  }

  DBHelper.internal();

  // USER TABLE FUNCTION
  Future<UserModel?> insertUser(UserModel user) async {
    var dbClient = await db;
    user.id = await dbClient!.insert(tableUser, user.toJson());
    return user;
  }

  Future<int?> updateUser(UserModel user) async {
    var dbClient = await db;
    return await dbClient!.update(tableUser, user.toJson(),
        where: '$columnId = ?', whereArgs: [user.id]);
  }

  Future<UserModel?> getUser(String email, String password) async {
    List<UserModel> user = [];
    var dbClient = await db;
    List<Map> maps = await dbClient!.query(tableUser,
        columns: [
          columnId,
          columnKodeDivisi,
          columnNama,
          columnEmail,
          columnPassword,
          columnNoTelp,
          columnRoles,
          columnUsername,
          columnJenisKelamin
        ],
        where: '$columnEmail = ? AND $columnPassword = ?',
        whereArgs: [email, password]);
    if (maps.length > 0) {
      maps.forEach((f) {
        user.add(UserModel.fromJson(f));
      });
    }
    return user.length != 0 ? user.first : null;
  }

  Future<UserModel?> getUserByID(int id) async {
    List<UserModel> user = [];
    var dbClient = await db;
    List<Map> maps = await dbClient!.query(tableUser,
        columns: [
          columnId,
          columnKodeDivisi,
          columnNama,
          columnEmail,
          columnPassword,
          columnNoTelp,
          columnRoles,
          columnUsername,
          columnJenisKelamin
        ],
        where: '$columnId = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      maps.forEach((f) {
        user.add(UserModel.fromJson(f));
      });
    }
    return user.length != 0 ? user.first : null;
  }

  Future<UserModel?> getUserByEmail(String email) async {
    List<UserModel> user = [];
    var dbClient = await db;
    List<Map> maps = await dbClient!.query(tableUser,
        columns: [
          columnId,
          columnKodeDivisi,
          columnNama,
          columnEmail,
          columnPassword,
          columnNoTelp,
          columnRoles,
          columnUsername,
          columnJenisKelamin
        ],
        where: '$columnEmail = ?',
        whereArgs: [email]);
    if (maps.length > 0) {
      maps.forEach((f) {
        user.add(UserModel.fromJson(f));
      });
    }
    return user.length != 0 ? user.first : null;
  }

  // ROLE TABLE FUNCTION
  Future<RolesModel?> insertRole(RolesModel role) async {
    var dbClient = await db;
    role.id = await dbClient!.insert(tableRole, role.toJson());
    return role;
  }

  Future<int?> updateRole(RolesModel role) async {
    var dbClient = await db;
    return await dbClient!.update(tableRole, role.toJson(),
        where: '$columnId = ?', whereArgs: [role.id]);
  }

  Future<RolesModel?> getRole(int roleId) async {
    List<RolesModel> roles = [];
    var dbClient = await db;
    List<Map> maps = await dbClient!.query(tableRole,
        columns: [columnId, columnNamaRole],
        where: '$columnId = ?',
        whereArgs: [roleId]);
    if (maps.length > 0) {
      maps.forEach((f) {
        roles.add(RolesModel.fromJson(f));
      });
    }
    return roles.length != 0 ? roles.first : null;
  }

  Future<List<RolesModel>?> getAllRole() async {
    List<RolesModel>? roles = [];
    var dbClient = await db;
    List<Map> maps = await dbClient!.query(
      tableRole,
      columns: [columnId, columnNamaRole],
    );
    if (maps.length > 0) {
      maps.forEach((f) {
        roles.add(RolesModel.fromJson(f));
      });
    }
    return roles;
  }

  // DIVISI TABLE FUNCTION
  Future<DivisiModel?> insertDivisi(DivisiModel division) async {
    var dbClient = await db;
    division.id = await dbClient!.insert(tableDivisi, division.toJson());
    return division;
  }

  Future<int?> updateDivisi(DivisiModel division) async {
    var dbClient = await db;
    return await dbClient!.update(tableDivisi, division.toJson(),
        where: '$columnId = ?', whereArgs: [division.id]);
  }

  Future<DivisiModel?> getDivisi(int divisionId) async {
    List<DivisiModel> listDivision = [];
    var dbClient = await db;
    List<Map> maps = await dbClient!.query(tableDivisi,
        columns: [columnId, columnNamaDivisi],
        where: '$columnId = ?',
        whereArgs: [divisionId]);
    if (maps.length > 0) {
      maps.forEach((f) {
        listDivision.add(DivisiModel.fromJson(f));
      });
    }
    return listDivision.length != 0 ? listDivision.first : null;
  }

  Future<int> deleteDivisi(int divisionId) async {
    var dbClient = await db;
    return await dbClient!
        .delete(tableDivisi, where: '$columnId = ?', whereArgs: [divisionId]);
  }

  Future<List<DivisiModel>?> getAllDivisi() async {
    List<DivisiModel> listDivision = [];
    var dbClient = await db;
    List<Map> maps = await dbClient!.query(
      tableDivisi,
      columns: [columnId, columnNamaDivisi],
    );
    if (maps.length > 0) {
      maps.forEach((f) {
        listDivision.add(DivisiModel.fromJson(f));
      });
    }
    return listDivision;
  }

  // KATEGORI KPI TABLE FUNCTION
  Future<KategoriKpiModel?> insertKategoriKPI(
      KategoriKpiModel kategoriKpiModel) async {
    var dbClient = await db;
    kategoriKpiModel.id =
        await dbClient!.insert(tableKategoriKPI, kategoriKpiModel.toJson());
    return kategoriKpiModel;
  }

  Future<int?> updateKategoriKPI(KategoriKpiModel kategoriKpiModel) async {
    var dbClient = await db;
    return await dbClient!.update(tableKategoriKPI, kategoriKpiModel.toJson(),
        where: '$columnId = ?', whereArgs: [kategoriKpiModel.id]);
  }

  Future<KategoriKpiModel?> getKategoriKPI(int kategoriID) async {
    List<KategoriKpiModel> listKpi = [];
    var dbClient = await db;
    List<Map> maps = await dbClient!.query(tableKategoriKPI,
        columns: [
          columnId,
          columnKodeDivisi,
          columnTotalKPI,
          columnTarget,
          columnKategori,
        ],
        where: '$columnId = ?',
        whereArgs: [kategoriID]);
    if (maps.length > 0) {
      maps.forEach((f) {
        listKpi.add(KategoriKpiModel.fromJson(f));
      });
    }
    return listKpi.length != 0 ? listKpi.first : null;
  }

  Future<List<KategoriKpiModel>?> getKategoriKPIByDivision(
      int divisionId) async {
    List<KategoriKpiModel> listKpi = [];
    var dbClient = await db;
    List<Map> maps = await dbClient!.query(tableKategoriKPI,
        columns: [
          columnId,
          columnKodeDivisi,
          columnTotalKPI,
          columnTarget,
          columnKategori,
        ],
        where: '$columnKodeDivisi = ?',
        whereArgs: [divisionId]);
    if (maps.length > 0) {
      maps.forEach((f) {
        listKpi.add(KategoriKpiModel.fromJson(f));
      });
    }
    return listKpi;
  }

  Future<List<KategoriKpiModel>?> getAllKategoriKPI() async {
    List<KategoriKpiModel> listKpi = [];
    var dbClient = await db;
    List<Map> maps = await dbClient!.query(
      tableKategoriKPI,
      columns: [
        columnId,
        columnKodeDivisi,
        columnTotalKPI,
        columnTarget,
        columnKategori,
      ],
    );
    if (maps.length > 0) {
      maps.forEach((f) {
        print(f.toString());
        listKpi.add(KategoriKpiModel.fromJson(f));
      });
    }
    return listKpi;
  }

  Future<int> deleteKPI(int kategoriID) async {
    var dbClient = await db;
    return await dbClient!.delete(tableKategoriKPI,
        where: '$columnId = ?', whereArgs: [kategoriID]);
  }

  // NILAI KPI  TABLE FUNCTION
  Future<NilaiKpiModel?> insertNilaiKPI(NilaiKpiModel nilaiKpiModel) async {
    var dbClient = await db;
    nilaiKpiModel.id =
        await dbClient!.insert(tableNilaiKPI, nilaiKpiModel.toJson());
    return nilaiKpiModel;
  }

  Future<int?> updateNilaiKPI(NilaiKpiModel nilaiKpiModel) async {
    var dbClient = await db;
    return await dbClient!.update(tableNilaiKPI, nilaiKpiModel.toJson(),
        where: '$columnId = ?', whereArgs: [nilaiKpiModel.id]);
  }

  Future<List<NilaiKpiModel>?> getAllNilaiKPI() async {
    List<NilaiKpiModel> listNilaiKPI = [];
    var dbClient = await db;
    List<Map> maps = await dbClient!.query(
      tableNilaiKPI,
      columns: [
        columnId,
        columnKodeDivisi,
        columnIdKategory,
        columnSkorKPI,
        columnIdKaryawan,
        columnBukti,
        columnStatus,
        columnLock,
        columnTanggal,
        columnAlasan
      ],
    );
    if (maps.length > 0) {
      maps.forEach((f) {
        listNilaiKPI.add(NilaiKpiModel.fromJson(f));
      });
    }
    return listNilaiKPI;
  }

  Future<NilaiKpiModel?> getNilaiKPI(int nilaiKPIid) async {
    List<NilaiKpiModel> listNilaiKPI = [];
    var dbClient = await db;
    List<Map> maps = await dbClient!.query(tableNilaiKPI,
        columns: [
          columnId,
          columnKodeDivisi,
          columnIdKategory,
          columnSkorKPI,
          columnIdKaryawan,
          columnBukti,
          columnStatus,
          columnLock,
          columnTanggal,
          columnAlasan
        ],
        where: '$columnId = ?',
        whereArgs: [nilaiKPIid]);
    if (maps.length > 0) {
      maps.forEach((f) {
        listNilaiKPI.add(NilaiKpiModel.fromJson(f));
      });
    }
    return listNilaiKPI.length != 0 ? listNilaiKPI.first : null;
  }

  Future<List<NilaiKpiModel>?> getNilaiKPIByEmployeID(int employeId) async {
    List<NilaiKpiModel> listNilaiKPI = [];
    var dbClient = await db;
    List<Map> maps = await dbClient!.query(tableNilaiKPI,
        columns: [
          columnId,
          columnKodeDivisi,
          columnIdKategory,
          columnSkorKPI,
          columnIdKaryawan,
          columnBukti,
          columnStatus,
          columnLock,
          columnTanggal,
          columnAlasan
        ],
        where: '$columnIdKaryawan = ?',
        whereArgs: [employeId]);
    if (maps.length > 0) {
      maps.forEach((f) {
        listNilaiKPI.add(NilaiKpiModel.fromJson(f));
      });
    }
    return listNilaiKPI;
  }

  Future<List<NilaiKpiModel>?> getNilaiKPIByDivision(int divisionId) async {
    List<NilaiKpiModel> listNilaiKPI = [];
    var dbClient = await db;
    List<Map> maps = await dbClient!.query(tableNilaiKPI,
        columns: [
          columnId,
          columnKodeDivisi,
          columnIdKategory,
          columnSkorKPI,
          columnIdKaryawan,
          columnBukti,
          columnStatus,
          columnLock,
          columnTanggal,
          columnAlasan
        ],
        where: '$columnKodeDivisi = ?',
        whereArgs: [divisionId]);
    if (maps.length > 0) {
      maps.forEach((f) {
        listNilaiKPI.add(NilaiKpiModel.fromJson(f));
      });
    }
    return listNilaiKPI;
  }

  Future<int?> deleteAllNilaiKPI() async {
    var dbClient = await db;
    return await dbClient!.delete(tableNilaiKPI);
  }

  Future<int> deleteNilaiKPI(int nilaiKpiId) async {
    var dbClient = await db;
    return await dbClient!
        .delete(tableNilaiKPI, where: '$columnId = ?', whereArgs: [nilaiKpiId]);
  }

  //
  //
  //
  Future close() async {
    var dbClient = await db;
    dbClient!.close();
  }
}
