import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../core/failures/failures.dart';
import '../../../../../core/models/divisi_model.dart';
import '../../../../../core/models/kategori_kpi_model.dart';
import '../../../../../core/models/nilai_kpi_model.dart';
import '../../../../../core/models/user_model.dart';
import '../../../../utils/app_color.dart';
import '../../data/home_repository.dart';
import '../../domain/entity/avg_perf_model.dart';
import '../../domain/entity/chart_model.dart';

class HomeController extends GetxController {
  final HomeRepository _homeRepository = HomeRepository();
  List<ChartModel> listChartData = [];
  List<DivisiModel> listDivisi = [];
  List<NilaiKpiModel> listNilaiKpi = [];
  List<Color> listChartColor = [
    AppColor.darkRedColor,
    AppColor.darkBlueColor,
    AppColor.yellowColor,
    AppColor.greyColor,
    AppColor.statusConfirmColor,
    AppColor.grapeColor,
  ];
  List<LineChartBarData> lineChartBarData = [];
  List<AvgPerfModel> divisionPerfList = [];
  List<AvgPerfModel> personalPerfList = [];
  List<KategoriKpiModel> listKategori = [];
  UserModel? userModel;

  @override
  void onInit() {
    getData();
    super.onInit();
  }

  getData() async {
    try {
      listDivisi = (await _homeRepository.getAllDivisi())!;
      listNilaiKpi = (await _homeRepository.getAllNilaiKPI())!;

      if (userModel == null) {
        userModel = await _homeRepository.getCurrentUser();
      }

      listKategori = (await _homeRepository
          .getKategoriKPIByDivision(userModel!.kodeDivisi))!;
      listChartData.clear();
      lineChartBarData.clear();

      listDivisi.forEach((element) {
        listChartData
            .add(ChartModel(divisionName: element.namaDivisi, listValue: []));
      });

      listDivisi.forEach((element) {
        for (var i = 0; i < listNilaiKpi.length; i++) {
          if (listNilaiKpi[i].kodeDivisi == element.id) {
            for (var item in listChartData) {
              if (element.namaDivisi == item.divisionName) {
                listNilaiKpi[i].skorKpi.forEach((e) {
                  item.listValue!.add(ValueModel(x: i, y: e));
                });
              }
            }
          }
        }
      });

      for (var i = 0; i < listChartData.length; i++) {
        List<FlSpot> listFlSpot = [];

        for (var j = 0; j < listChartData[i].listValue!.length; j++) {
          listFlSpot.add(FlSpot(
              j.toDouble(), listChartData[i].listValue![j].y.toDouble()));
        }

        lineChartBarData.add(LineChartBarData(
          spots: listFlSpot,
          isCurved: true,
          colors: [listChartColor[i]],
          barWidth: 8,
          isStrokeCapRound: true,
          dotData: FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: false,
          ),
        ));
      }

      _computeAvgDivisionPerformance();
      _computerAvgPersonalPerformance();

      update();
    } on Failures catch (e) {
      Get.showSnackbar(GetBar(
        title: "Error",
        message: e.toString(),
      ));
    }
  }

  _computeAvgDivisionPerformance() {
    divisionPerfList.clear();

    if (listChartData[0].listValue!.isNotEmpty) {
      for (var i = 0; i < listChartData.length; i++) {
        if (listChartData[i].listValue != null) {
          int avg = 0;

          for (var item in listChartData[i].listValue!) {
            avg = avg + item.y;
          }

          divisionPerfList.add(AvgPerfModel(
              title: listChartData[i].divisionName,
              color: listChartColor[i],
              avgScore: avg == 0
                  ? avg
                  : (avg ~/ listChartData[i].listValue!.length)));
        }
      }
    }
  }

  _computerAvgPersonalPerformance() {
    personalPerfList.clear();

    listNilaiKpi.forEach((data) {
      listKategori.forEach((element) {
        if (data.idKaryawan == userModel!.id) {
          data.idKategory.asMap().entries.forEach((category) {
            if (category.value == element.id) {
              int avg = 0;
              avg += data.skorKpi[category.key];
              personalPerfList.add(AvgPerfModel(
                  title: element.kategoriKpi,
                  avgScore: avg,
                  target: element.target));
            }
          });
        }
      });
    });

    /*
    for (var i = 0; i < personalPerfList.length; i++) {
      for (var item in personalPerfList) {
        personalPerfList[i].avgScore =
            personalPerfList[i].avgScore! + item.avgScore!;
      }
    }*/

    final ids = personalPerfList.map((e) => e.title).toSet();
    personalPerfList.retainWhere((x) => ids.remove(x.title));
  }
}
