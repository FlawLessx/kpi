import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/state_manager.dart';

import '../../../../utils/app_color.dart';
import '../controller/home_controller.dart';
import 'widget/division_performance_chart.dart';
import 'widget/personal_performance.dart';

class HomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
        init: HomeController(),
        builder: (controller) => Scaffold(
              appBar: null,
              body: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.only(
                      left: ScreenUtil().setHeight(20),
                      right: ScreenUtil().setHeight(20),
                      bottom: ScreenUtil().setHeight(20)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Performa Pribadi',
                        style: TextStyle(
                          color: AppColor.blackColor,
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      PersonalPerformance(controller: controller),
                      SizedBox(height: ScreenUtil().setHeight(20)),
                      Text(
                        'Performa Divisi',
                        style: TextStyle(
                          color: AppColor.blackColor,
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      DivisionPerformanceChart(controller: controller)
                    ],
                  ),
                ),
              ),
            ));
  }
}
