import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../../utils/app_color.dart';
import '../../controller/home_controller.dart';

class DivisionPerformanceChart extends StatelessWidget {
  final HomeController controller;

  DivisionPerformanceChart({required this.controller});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(ScreenUtil().setHeight(10)),
      decoration: BoxDecoration(
          color: AppColor.blackColor,
          borderRadius: BorderRadius.circular(ScreenUtil().setHeight(10))),
      child: Stack(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(ScreenUtil().setHeight(10)),
                child: Container(
                  height: ScreenUtil().setHeight(250),
                  padding: EdgeInsets.symmetric(
                      vertical: ScreenUtil().setHeight(20)),
                  decoration: BoxDecoration(
                      color: AppColor.blackSecondaryColor,
                      borderRadius:
                          BorderRadius.circular(ScreenUtil().setHeight(10))),
                  child: Padding(
                    padding: EdgeInsets.only(
                        left: ScreenUtil().setHeight(10),
                        right: ScreenUtil().setHeight(20)),
                    child: LineChart(
                      LineChartData(
                        lineTouchData: LineTouchData(
                          touchTooltipData: LineTouchTooltipData(
                            tooltipBgColor: Colors.white.withOpacity(0.7),
                          ),
                          touchCallback: (LineTouchResponse touchResponse) {},
                          handleBuiltInTouches: true,
                        ),
                        gridData: FlGridData(
                          show: false,
                        ),
                        titlesData: FlTitlesData(
                          bottomTitles: SideTitles(
                            showTitles: true,
                            reservedSize: 22,
                            getTextStyles: (value) => const TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                            margin: 10,
                            getTitles: (value) {
                              switch (value.toInt()) {
                                case 2:
                                  return 'Mei';
                                case 6:
                                  return 'Juni';
                                case 10:
                                  return 'Juli';
                              }
                              return '';
                            },
                          ),
                          leftTitles: SideTitles(
                            showTitles: true,
                            getTextStyles: (value) => const TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 14,
                            ),
                            getTitles: (value) {
                              switch (value.toInt()) {
                                case 5:
                                  return '5';
                                case 20:
                                  return '20';
                                case 40:
                                  return '40';
                                case 60:
                                  return '60';
                                case 80:
                                  return '80';
                                case 100:
                                  return '100+';
                              }
                              return '';
                            },
                            margin: 8,
                            reservedSize: 30,
                          ),
                        ),
                        borderData: FlBorderData(
                          show: true,
                          border: const Border(
                            bottom: BorderSide(
                              color: AppColor.blackColor,
                              width: 4,
                            ),
                            left: BorderSide(
                              color: Colors.transparent,
                            ),
                            right: BorderSide(
                              color: Colors.transparent,
                            ),
                            top: BorderSide(
                              color: Colors.transparent,
                            ),
                          ),
                        ),
                        minX: 0,
                        maxX: 10,
                        maxY: 110,
                        minY: 0,
                        lineBarsData: controller.lineChartBarData,
                      ),
                      swapAnimationDuration: const Duration(milliseconds: 250),
                    ),
                  ),
                ),
              ),
              ListView.builder(
                  itemCount: controller.divisionPerfList.length,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setHeight(10)),
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: EdgeInsets.only(
                          top: ScreenUtil().setHeight(index != 0 ? 5 : 0)),
                      child: Row(
                        children: [
                          Text(
                            controller.divisionPerfList[index].title!,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w500),
                          ),
                          SizedBox(width: ScreenUtil().setHeight(5)),
                          Container(
                            height: ScreenUtil().setHeight(13),
                            width: ScreenUtil().setHeight(13),
                            decoration: BoxDecoration(
                                color: controller.divisionPerfList[index].color,
                                borderRadius: BorderRadius.circular(
                                    ScreenUtil().setHeight(30))),
                          ),
                          Text(
                            " : ${controller.divisionPerfList[index].avgScore} (Avg Score)",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    );
                  })
            ],
          ),
        ],
      ),
    );
  }
}
