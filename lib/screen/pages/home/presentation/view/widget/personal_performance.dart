import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../../utils/app_color.dart';
import '../../../../admin/presentation/view/widget/container_item.dart';
import '../../controller/home_controller.dart';

class PersonalPerformance extends StatelessWidget {
  final HomeController controller;

  PersonalPerformance({required this.controller});

  @override
  Widget build(BuildContext context) {
    return containerItem(
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListView.builder(
                itemCount: controller.personalPerfList.length,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                padding: EdgeInsets.symmetric(
                    horizontal: ScreenUtil().setHeight(10)),
                itemBuilder: (context, index) {
                  return Padding(
                    padding: EdgeInsets.only(
                        top: ScreenUtil().setHeight(index != 0 ? 5 : 0)),
                    child: Row(
                      children: [
                        Flexible(
                          child: Text(
                            controller.personalPerfList[index].title!,
                            style: TextStyle(
                                color: AppColor.blackColor,
                                fontSize: 16,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                        SizedBox(width: ScreenUtil().setHeight(5)),
                        Flexible(
                          child: Text(
                            " : ${controller.personalPerfList[index].avgScore} (Avg Score)",
                            style: TextStyle(
                                color: AppColor.blackColor,
                                fontSize: 16,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                      ],
                    ),
                  );
                })
          ],
        ),
        padding: 10);
  }
}
