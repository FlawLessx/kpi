import 'dart:convert';

import 'package:sqflite/sqflite.dart';

import '../../../../core/datasources/database/db_helper.dart';
import '../../../../core/datasources/pref/pref_manager.dart';
import '../../../../core/failures/failures.dart';
import '../../../../core/models/divisi_model.dart';
import '../../../../core/models/kategori_kpi_model.dart';
import '../../../../core/models/nilai_kpi_model.dart';
import '../../../../core/models/user_model.dart';
import '../../../../service_locator.dart';

abstract class IHomeRepository {
  Future<List<DivisiModel>?> getAllDivisi();
  Future<List<NilaiKpiModel>?> getAllNilaiKPI();
  Future<List<KategoriKpiModel>?> getKategoriKPIByDivision(int divisionId);
  Future<UserModel?> getCurrentUser();
}

class HomeRepository implements IHomeRepository {
  late final DBHelper _dbHelper = getIt.get<DBHelper>();
  final PrefsManager _prefsManager = getIt.get<PrefsManager>();

  @override
  Future<List<DivisiModel>?> getAllDivisi() async {
    try {
      return await _dbHelper.getAllDivisi();
    } on DatabaseException {
      throw Failures("Failed Get All Division Data");
    }
  }

  @override
  Future<List<NilaiKpiModel>?> getAllNilaiKPI() async {
    try {
      return await _dbHelper.getAllNilaiKPI();
    } on DatabaseException {
      throw Failures("Failed Get All KPI");
    }
  }

  @override
  Future<List<KategoriKpiModel>?> getKategoriKPIByDivision(
      int divisionId) async {
    try {
      return await _dbHelper.getKategoriKPIByDivision(divisionId);
    } on DatabaseException {
      throw Failures("Failed Get All Category");
    }
  }

  @override
  Future<UserModel?> getCurrentUser() async {
    try {
      final data = await _prefsManager.getString('user');
      return UserModel.fromJson(jsonDecode(data!));
    } on DatabaseException {
      throw Failures("Failed Get All Category");
    }
  }
}
