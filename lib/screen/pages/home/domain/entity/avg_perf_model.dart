import 'package:flutter/material.dart';

class AvgPerfModel {
  String? title;
  int? avgScore;
  Color? color;
  int? target;

  AvgPerfModel(
      {required this.title, required this.avgScore, this.color, this.target});

  AvgPerfModel.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    avgScore = json['avgScore'];
    target = json['target'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['avgScore'] = this.avgScore;
    data['target'] = this.target;
    return data;
  }
}
