class ChartModel {
  final String divisionName;
  List<ValueModel>? listValue;

  ChartModel({required this.divisionName, this.listValue});
}

class ValueModel {
  final int x;
  final int y;

  ValueModel({required this.x, required this.y});
}
