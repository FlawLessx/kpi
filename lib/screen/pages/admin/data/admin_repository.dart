import 'dart:convert';

import '../../../../core/datasources/database/db_helper.dart';
import '../../../../core/datasources/pref/pref_manager.dart';
import '../../../../core/failures/failures.dart';
import '../../../../core/models/divisi_model.dart';
import '../../../../core/models/kategori_kpi_model.dart';
import '../../../../core/models/nilai_kpi_model.dart';
import '../../../../core/models/user_model.dart';
import '../../../../service_locator.dart';
import 'package:sqflite/sqflite.dart';

abstract class IAdminRepository {
  Future<List<DivisiModel>?> getAllDivisi();
  Future<DivisiModel?> insertDivisi(DivisiModel division);
  Future<int> deleteDivisi(int divisionId);
  Future<int> deleteKPI(int kategoriID);
  Future<KategoriKpiModel?> getKategoriKPI(int kategoriID);
  Future<List<KategoriKpiModel>?> getAllKategoriKPI();
  Future<int?> updateKategoriKPI(KategoriKpiModel kategoriKpiModel);
  Future<KategoriKpiModel?> insertKategoriKPI(
      KategoriKpiModel kategoriKpiModel);
  Future<List<KategoriKpiModel>?> getKategoriKPIByDivision(int divisionId);
  Future<UserModel> getCurrentUser();
  Future<List<NilaiKpiModel>?> getNilaiKPIByDivision(int divisionId);
  Future<List<NilaiKpiModel>?> getAllNilaiKPI();
  Future<UserModel?> getUserByID(int id);
  Future<int?> updateNilaiKPI(NilaiKpiModel nilaiKpiModel);
  Future<int> deleteNilaiKPI(int nilaiKpiId);
}

class AdminRepository implements IAdminRepository {
  late final DBHelper _dbHelper = getIt.get<DBHelper>();
  final PrefsManager _prefsManager = getIt.get<PrefsManager>();

  @override
  Future<List<DivisiModel>?> getAllDivisi() async {
    try {
      return await _dbHelper.getAllDivisi();
    } on DatabaseException {
      throw Failures("Failed Get All Division Data");
    }
  }

  @override
  Future<DivisiModel?> insertDivisi(DivisiModel division) async {
    try {
      return await _dbHelper.insertDivisi(division);
    } on DatabaseException {
      throw Failures("Failed Create Division");
    }
  }

  @override
  Future<int> deleteDivisi(int divisionId) async {
    try {
      return await _dbHelper.deleteDivisi(divisionId);
    } on DatabaseException {
      throw Failures("Failed Delete Division");
    }
  }

  @override
  Future<int> deleteKPI(int kategoriID) async {
    try {
      return await _dbHelper.deleteKPI(kategoriID);
    } on DatabaseException {
      throw Failures("Failed Delete KPI");
    }
  }

  @override
  Future<KategoriKpiModel?> insertKategoriKPI(
      KategoriKpiModel kategoriKpiModel) async {
    try {
      return await _dbHelper.insertKategoriKPI(kategoriKpiModel);
    } on DatabaseException {
      throw Failures("Failed Create KPI");
    }
  }

  @override
  Future<int?> updateKategoriKPI(KategoriKpiModel kategoriKpiModel) async {
    try {
      return await _dbHelper.updateKategoriKPI(kategoriKpiModel);
    } on DatabaseException {
      throw Failures("Failed Update KPI");
    }
  }

  @override
  Future<KategoriKpiModel?> getKategoriKPI(int kategoriID) async {
    try {
      return await _dbHelper.getKategoriKPI(kategoriID);
    } on DatabaseException {
      throw Failures("Failed Get KPI");
    }
  }

  @override
  Future<List<KategoriKpiModel>?> getAllKategoriKPI() async {
    try {
      return await _dbHelper.getAllKategoriKPI();
    } on DatabaseException {
      throw Failures("Failed Get All KPI");
    }
  }

  @override
  Future<List<KategoriKpiModel>?> getKategoriKPIByDivision(
      int divisionId) async {
    try {
      return await _dbHelper.getKategoriKPIByDivision(divisionId);
    } on DatabaseException {
      throw Failures("Failed Get KPI By Division");
    }
  }

  @override
  Future<UserModel> getCurrentUser() async {
    try {
      final data = await _prefsManager.getString('user');
      return UserModel.fromJson(jsonDecode(data!));
    } on DatabaseException {
      throw Failures("Failed Get Current User");
    }
  }

  @override
  Future<List<NilaiKpiModel>?> getNilaiKPIByDivision(int divisionId) async {
    try {
      return await _dbHelper.getNilaiKPIByDivision(divisionId);
    } on DatabaseException {
      throw Failures("Failed Get KPI By Division");
    }
  }

  @override
  Future<List<NilaiKpiModel>?> getAllNilaiKPI() async {
    try {
      return await _dbHelper.getAllNilaiKPI();
    } on DatabaseException {
      throw Failures("Failed Get All KPI");
    }
  }

  @override
  Future<UserModel?> getUserByID(int id) async {
    try {
      return await _dbHelper.getUserByID(id);
    } on DatabaseException {
      throw Failures("Failed Get KPI By User");
    }
  }

  @override
  Future<int?> updateNilaiKPI(NilaiKpiModel nilaiKpiModel) async {
    try {
      return await _dbHelper.updateNilaiKPI(nilaiKpiModel);
    } on DatabaseException {
      throw Failures("Failed Get KPI By User");
    }
  }

  @override
  Future<int> deleteNilaiKPI(int nilaiKpiId) async {
    try {
      return await _dbHelper.deleteNilaiKPI(nilaiKpiId);
    } on DatabaseException {
      throw Failures("Failed Delete Nilai KPI");
    }
  }
}
