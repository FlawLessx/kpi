class DetailKPIModel {
  final int id;
  final String nama;
  final String namaDivisi;
  final String tanggal;
  final List<String> kpiName;
  final List<int> kpiSkor;
  final String image;
  final String status;
  final String alasan;

  DetailKPIModel(
      {required this.id,
      required this.nama,
      required this.namaDivisi,
      required this.tanggal,
      required this.kpiName,
      required this.kpiSkor,
      required this.image,
      required this.status,
      required this.alasan});
}
