import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';

import '../../../../shared/widget/custom_textfield_2.dart';
import '../../../../utils/app_color.dart';
import '../controller/admin_controller.dart';
import 'widget/container_item.dart';
import 'widget/delete_confirmation_dialog.dart';

class AdminDivisiWidget extends StatelessWidget {
  final AdminController adminController;

  AdminDivisiWidget({required this.adminController});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AdminController>(
        init: adminController,
        builder: (controller) => SafeArea(
              child: Scaffold(
                body: ListView(
                  padding: EdgeInsets.all(ScreenUtil().setHeight(20)),
                  children: [
                    addDivisi(controller),
                    SizedBox(
                      height: ScreenUtil().setHeight(20),
                    ),
                    listDivisi(controller)
                  ],
                ),
              ),
            ));
  }

  Widget addDivisi(AdminController controller) {
    return containerItem(
      ExpandableNotifier(
        initialExpanded: false,
        child: ScrollOnExpand(
            child: ExpandablePanel(
          header: Text(
            "Tambah Divisi",
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600,
                color: AppColor.blackColor),
          ),
          collapsed: Text(
            "Kolom untuk menambahkan divisi",
            style: TextStyle(
                fontSize: 13, color: Colors.grey, fontWeight: FontWeight.w400),
          ),
          expanded:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Form(
              key: controller.divisiFormKey,
              child: CustomTextfield2(
                controller: controller.addDivisionController,
                labelText: "Nama Divisi",
                validator: (value) {
                  if (value == "" || value == null) {
                    return "Nama divisi is required";
                  }

                  return null;
                },
              ),
            ),
            SizedBox(
              height: ScreenUtil().setHeight(20),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    width: ScreenUtil().setHeight(200),
                    child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) {
                              return AppColor
                                  .mainColor; // Use the component's default.
                            },
                          ),
                        ),
                        onPressed: () {
                          controller.createDivision();
                        },
                        child: Center(
                          child: Text("Tambah"),
                        ))),
              ],
            )
          ]),
        )),
      ),
    );
  }

  Widget listDivisi(AdminController controller) {
    return containerItem(
      ExpandableNotifier(
        initialExpanded: true,
        child: ScrollOnExpand(
            child: ExpandablePanel(
          header: Text(
            "List Divisi",
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600,
                color: AppColor.blackColor),
          ),
          collapsed: Text(
            "Kolom untuk melihat semua divisi",
            style: TextStyle(
                fontSize: 13, color: Colors.grey, fontWeight: FontWeight.w400),
          ),
          expanded: Padding(
            padding: EdgeInsets.all(ScreenUtil().setHeight(10)),
            child: ListView.builder(
                itemCount: controller.listDivisi!.length,
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (context, index) => Padding(
                      padding: index == 0
                          ? EdgeInsets.all(0)
                          : EdgeInsets.only(top: ScreenUtil().setHeight(10)),
                      child: Container(
                        width: double.maxFinite,
                        padding: EdgeInsets.all(ScreenUtil().setHeight(10)),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(
                                ScreenUtil().setHeight(10)),
                            boxShadow: [
                              BoxShadow(
                                  offset: Offset(0, 0),
                                  spreadRadius: 2,
                                  blurRadius: 1,
                                  color:
                                      AppColor.secondaryColor.withOpacity(0.5))
                            ]),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              controller.listDivisi![index].namaDivisi,
                              style: TextStyle(
                                  color: AppColor.blackColor,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500),
                            ),
                            InkWell(
                              onTap: () {
                                Get.dialog(DeleteConfirmationDialog(
                                    title: 'divisi',
                                    subtitle: controller
                                        .listDivisi![index].namaDivisi,
                                    deleteFunction: () {
                                      controller.deleteDivision(
                                          controller.listDivisi![index].id!);
                                      Get.back();
                                    }));
                              },
                              child: Icon(
                                Icons.delete,
                                color: Colors.redAccent,
                              ),
                            )
                          ],
                        ),
                      ),
                    )),
          ),
        )),
      ),
    );
  }
}
