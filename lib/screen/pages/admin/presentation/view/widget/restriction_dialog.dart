import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../../shared/widget/custom_button_2.dart';
import '../../../../../utils/app_color.dart';

class RestrictionDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        "Informasi",
        style: TextStyle(
            color: AppColor.blackColor,
            fontWeight: FontWeight.bold,
            fontSize: 18),
      ),
      content: Text("Anda tidak memiliki akses untuk melakukan ini"),
      actions: [
        CustomButton2(
          title: "OK",
          onTap: () {
            Get.back();
          },
        )
      ],
    );
  }
}
