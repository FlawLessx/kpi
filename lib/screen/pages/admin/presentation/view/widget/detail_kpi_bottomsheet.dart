import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kpi_system/screen/pages/admin/presentation/controller/admin_controller.dart';

import '../../../../../shared/widget/custom_button_2.dart';
import '../../../../../utils/app_color.dart';
import '../../../domain/entity/detail_kpi_model.dart';
import 'container_item.dart';

class DetailKPIBottomSheet extends StatefulWidget {
  final DetailKPIModel detailKPIModel;
  final AdminController adminController;

  DetailKPIBottomSheet(
      {required this.detailKPIModel, required this.adminController});

  @override
  _DetailKPIBottomSheetState createState() => _DetailKPIBottomSheetState();
}

class _DetailKPIBottomSheetState extends State<DetailKPIBottomSheet> {
  bool isDecline = false;
  bool isAlasanNull = false;
  TextEditingController _alasanController = TextEditingController();

  @override
  void initState() {
    _getStatus();
    super.initState();
  }

  _getStatus() {
    if (widget.detailKPIModel.status == "Ditolak") {
      isDecline = true;
    }
    _alasanController.text = widget.detailKPIModel.alasan;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(ScreenUtil().setHeight(40)),
              topRight: Radius.circular(ScreenUtil().setHeight(40)))),
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: ScreenUtil().setHeight(20),
              vertical: ScreenUtil().setHeight(20)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Detail Nilai KPI",
                style: TextStyle(
                    color: AppColor.blackColor,
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: ScreenUtil().setHeight(20),
              ),
              item("Nama: ", widget.detailKPIModel.nama),
              SizedBox(
                height: ScreenUtil().setHeight(10),
              ),
              item("Divisi: ", widget.detailKPIModel.namaDivisi),
              SizedBox(
                height: ScreenUtil().setHeight(10),
              ),
              item("Tanggal: ", widget.detailKPIModel.tanggal),
              SizedBox(
                height: ScreenUtil().setHeight(10),
              ),
              ListView.builder(
                  shrinkWrap: true,
                  padding: EdgeInsets.zero,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: widget.detailKPIModel.kpiSkor.length,
                  itemBuilder: (context, index) => Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          item(widget.detailKPIModel.kpiName[index] + ":",
                              widget.detailKPIModel.kpiSkor[index].toString()),
                          SizedBox(
                            height: ScreenUtil().setHeight(10),
                          ),
                        ],
                      )),
              Text("Bukti Pendukung: ",
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color: AppColor.blackColor)),
              SizedBox(
                height: ScreenUtil().setHeight(5),
              ),
              containerItem(
                  Image.file(
                    File(widget.detailKPIModel.image),
                    width: double.infinity,
                    height: ScreenUtil().setHeight(150),
                    fit: BoxFit.cover,
                  ),
                  padding: 10),
              SizedBox(
                height: ScreenUtil().setHeight(10),
              ),
              Visibility(
                visible: isDecline,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Alasan Penolakan: ",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: AppColor.blackColor)),
                    SizedBox(
                      height: ScreenUtil().setHeight(5),
                    ),
                    containerItem(
                        TextField(
                          controller: _alasanController,
                          maxLines: 3,
                          decoration: InputDecoration(border: InputBorder.none),
                        ),
                        padding: 10),
                    Visibility(
                      visible: isAlasanNull,
                      child: Text("Harap Isi alasan penolakan",
                          style: TextStyle(
                              fontSize: 13, color: Colors.red.shade700)),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: ScreenUtil().setHeight(20),
              ),
              Visibility(
                visible: widget.detailKPIModel.status == "Disetujui" ||
                        widget.detailKPIModel.status == "Ditolak"
                    ? false
                    : true,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomButton2(
                      title: "Tolak",
                      onTap: () {
                        if (isDecline == false) {
                          isDecline = true;
                          setState(() {});
                        } else {
                          if (_alasanController.text != '') {
                            widget.adminController.updateStatusKPI(false,
                                alasan: _alasanController.text);
                          } else {
                            isAlasanNull = true;
                            setState(() {});
                          }
                        }
                      },
                      color: Colors.red,
                    ),
                    SizedBox(
                      width: ScreenUtil().setHeight(20),
                    ),
                    CustomButton2(
                        title: "Terima",
                        onTap: () {
                          widget.adminController.updateStatusKPI(true);
                        })
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget item(String title, String subtitle) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: Text(title,
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  color: AppColor.blackColor)),
        ),
        SizedBox(width: ScreenUtil().setHeight(20)),
        Text(subtitle,
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w400,
                color: AppColor.blackColor))
      ],
    );
  }
}
