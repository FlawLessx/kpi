import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:get/get.dart';

import '../../../../../utils/app_color.dart';
import '../../controller/admin_controller.dart';

class AdminDrawer extends StatefulWidget {
  final AdminController adminController;

  AdminDrawer({required this.adminController});

  @override
  _AdminDrawerState createState() => _AdminDrawerState();
}

class _AdminDrawerState extends State<AdminDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: Container(
      color: Colors.white,
      child: SafeArea(
          child: GetBuilder<AdminController>(
        init: widget.adminController,
        builder: (controller) => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            UserAccountsDrawerHeader(
                currentAccountPicture: Image.asset(
                  "src/img/img_default.png",
                  height: ScreenUtil().setHeight(100),
                  width: ScreenUtil().setHeight(100),
                ),
                accountName: Text(controller.userModel!.nama + " (Manager)"),
                accountEmail: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(controller.userModel!.email),
                    if (controller.userModel!.email != "admin@kpi.com")
                      Text("Division: " + controller.userDivision!),
                  ],
                )),
            Expanded(
              child: ListView.builder(
                  itemCount: controller.listDrawerItem.length,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) => ListTile(
                        tileColor: controller.selectedDrawerIndex == index
                            ? AppColor.blackColor
                            : Colors.white,
                        onTap: () {
                          controller.selectedDrawerIndex = index;
                          controller.update();
                          Get.back();
                          controller.listDrawerItem[index].function.call();
                        },
                        leading: Icon(
                          controller.listDrawerItem[index].icons,
                          color: controller.selectedDrawerIndex == index
                              ? Colors.white
                              : AppColor.blackColor,
                        ),
                        title: Text(
                          controller.listDrawerItem[index].title,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: controller.selectedDrawerIndex == index
                                ? Colors.white
                                : AppColor.blackColor,
                          ),
                        ),
                        trailing: Icon(
                          Icons.chevron_right,
                          color: controller.selectedDrawerIndex == index
                              ? Colors.white
                              : AppColor.blackColor,
                        ),
                      )),
            ),
            ListTile(
              tileColor: Colors.white,
              onTap: () {
                Get.back();
                controller.logout();
              },
              leading: Icon(
                MaterialCommunityIcons.logout,
                color: AppColor.blackColor,
              ),
              title: Text(
                "Logout",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  color: AppColor.blackColor,
                ),
              ),
              trailing: Icon(
                Icons.chevron_right,
                color: AppColor.blackColor,
              ),
            )
          ],
        ),
      )),
    ));
  }
}
