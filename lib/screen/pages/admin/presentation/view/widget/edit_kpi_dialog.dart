import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../../core/models/kategori_kpi_model.dart';
import '../../../../../shared/widget/custom_button_2.dart';
import '../../../../../shared/widget/custom_textfield_2.dart';
import '../../../../../utils/app_color.dart';
import '../../controller/admin_controller.dart';

class EditKPIDialog extends StatefulWidget {
  final KategoriKpiModel kategoriKpiModel;
  final AdminController controller;

  EditKPIDialog({
    required this.kategoriKpiModel,
    required this.controller,
  });

  @override
  _EditKPIDialogState createState() => _EditKPIDialogState();
}

class _EditKPIDialogState extends State<EditKPIDialog> {
  final TextEditingController kpiNama = TextEditingController();
  final TextEditingController kpiTarget = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    kpiNama.dispose();
    kpiTarget.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    kpiNama.text = widget.kategoriKpiModel.kategoriKpi;
    kpiTarget.text = widget.kategoriKpiModel.target.toString();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        "Edit KPI",
        style: TextStyle(
            color: AppColor.blackColor,
            fontWeight: FontWeight.bold,
            fontSize: 18),
      ),
      content: Form(
        key: formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            CustomTextfield2(
              controller: kpiNama,
              labelText: "Nama Kategori KPI",
              validator: (value) {
                if (value == "" || value == null) {
                  return "Nama kategori is required";
                }

                return null;
              },
            ),
            CustomTextfield2(
              controller: kpiTarget,
              labelText: "Target",
              useOnlyNumber: true,
              validator: (value) {
                if (value == "" || value == null) {
                  return "Target is required";
                }

                return null;
              },
            ),
          ],
        ),
      ),
      actions: [
        TextButton(
            onPressed: () {
              Get.back();
            },
            child: Text(
              'Cancel',
              style: TextStyle(color: Colors.red),
            )),
        CustomButton2(
            title: "OK",
            onTap: () {
              if (formKey.currentState!.validate()) {
                widget.controller.updateKPI(KategoriKpiModel(
                    id: widget.kategoriKpiModel.id,
                    kodeDivisi: widget.kategoriKpiModel.kodeDivisi,
                    kategoriKpi: kpiNama.text,
                    target: int.parse(kpiTarget.text)));

                Get.back();
              }
            })
      ],
    );
  }
}
