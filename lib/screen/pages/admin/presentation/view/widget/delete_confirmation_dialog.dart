import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../shared/widget/custom_button_2.dart';
import '../../../../../utils/app_color.dart';

class DeleteConfirmationDialog extends StatelessWidget {
  final String title;
  final String subtitle;
  final Function() deleteFunction;

  DeleteConfirmationDialog(
      {required this.title,
      required this.deleteFunction,
      required this.subtitle});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        "Konfirmasi",
        style: TextStyle(
            color: AppColor.blackColor,
            fontWeight: FontWeight.bold,
            fontSize: 18),
      ),
      content: Text("Apakah anda yakin ingin menghapus $title $subtitle"),
      actions: [
        TextButton(
            onPressed: () {
              Get.back();
            },
            child: Text(
              'Cancel',
              style: TextStyle(color: Colors.red),
            )),
        CustomButton2(title: "OK", onTap: deleteFunction)
      ],
    );
  }
}
