import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

Widget containerItem(Widget child, {double? padding}) {
  return Container(
    padding: EdgeInsets.all(padding == null ? 20 : padding),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(ScreenUtil().setHeight(15)),
        border: Border.all(color: Colors.grey.withOpacity(0.5), width: 1)),
    child: child,
  );
}
