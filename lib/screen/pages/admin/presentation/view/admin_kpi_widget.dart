import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';

import '../../../../../core/models/divisi_model.dart';
import '../../../../shared/widget/custom_textfield_2.dart';
import '../../../../utils/app_color.dart';
import '../controller/admin_controller.dart';
import 'widget/container_item.dart';
import 'widget/delete_confirmation_dialog.dart';
import 'widget/edit_kpi_dialog.dart';
import 'widget/restriction_dialog.dart';

class AdminKPIWidget extends StatelessWidget {
  final AdminController adminController;

  AdminKPIWidget({required this.adminController});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AdminController>(
        init: adminController,
        builder: (controller) => SafeArea(
              child: Scaffold(
                body: ListView(
                  padding: EdgeInsets.all(ScreenUtil().setHeight(20)),
                  children: [
                    addDivisi(controller),
                    SizedBox(
                      height: ScreenUtil().setHeight(20),
                    ),
                    listDivisi(controller)
                  ],
                ),
              ),
            ));
  }

  Widget addDivisi(AdminController controller) {
    return containerItem(
      ExpandableNotifier(
        initialExpanded: false,
        child: ScrollOnExpand(
            child: ExpandablePanel(
          header: Text(
            "Tambah KPI",
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600,
                color: AppColor.blackColor),
          ),
          collapsed: Text(
            "Kolom untuk menambahkan kategori KPI",
            style: TextStyle(
                fontSize: 13, color: Colors.grey, fontWeight: FontWeight.w400),
          ),
          expanded: Form(
            key: controller.kpiFormKey,
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              DropdownButton<DivisiModel>(
                value: controller.selectedDivision,
                items: controller.listDivisi!
                    .map((e) => DropdownMenuItem(
                          child: Text(
                            e.namaDivisi,
                            style: TextStyle(
                                fontSize: 16.0,
                                color: AppColor.blackColor,
                                fontWeight: FontWeight.w500),
                          ),
                          value: e,
                        ))
                    .toList(),
                onChanged: (value) {
                  controller.selectedDivision = value;
                  controller.update();
                },
                hint: Text(
                  "Select Division",
                  style: TextStyle(
                      fontSize: 16.0,
                      color: AppColor.blackColor,
                      fontWeight: FontWeight.w500),
                ),
              ),
              Visibility(
                visible: !controller.divisionSelected,
                child: Text(
                  "Please select division",
                  style: TextStyle(color: Colors.red.shade700, fontSize: 12),
                ),
              ),
              CustomTextfield2(
                controller: controller.kpiNamaController,
                labelText: "Nama Kategori KPI",
                validator: (value) {
                  if (value == "" || value == null) {
                    return "Nama kategori is required";
                  }

                  return null;
                },
              ),
              CustomTextfield2(
                controller: controller.kpiTargetController,
                labelText: "Target",
                useOnlyNumber: true,
                validator: (value) {
                  if (value == "" || value == null) {
                    return "Target is required";
                  }

                  return null;
                },
              ),
              SizedBox(
                height: ScreenUtil().setHeight(20),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      width: ScreenUtil().setHeight(200),
                      child: ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.resolveWith<Color>(
                              (Set<MaterialState> states) {
                                return AppColor
                                    .mainColor; // Use the component's default.
                              },
                            ),
                          ),
                          onPressed: () {
                            controller.createKategoriKPI();
                          },
                          child: Center(
                            child: Text("Tambah KPI"),
                          ))),
                ],
              )
            ]),
          ),
        )),
      ),
    );
  }

  Widget listDivisi(AdminController controller) {
    return containerItem(
      ExpandableNotifier(
        initialExpanded: true,
        child: ScrollOnExpand(
            child: ExpandablePanel(
          header: Text(
            "List KPI",
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600,
                color: AppColor.blackColor),
          ),
          collapsed: Text(
            "Kolom untuk melihat detail tiap KPI",
            style: TextStyle(
                fontSize: 13, color: Colors.grey, fontWeight: FontWeight.w400),
          ),
          expanded: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              DropdownButton<DivisiModel>(
                value: controller.selectedKPIDivision,
                items: controller.listDivisi!
                    .map((e) => DropdownMenuItem(
                          child: Text(
                            "KPI ${e.namaDivisi}",
                            style: TextStyle(
                                fontSize: 16.0,
                                color: AppColor.blackColor,
                                fontWeight: FontWeight.w500),
                          ),
                          value: e,
                        ))
                    .toList(),
                onChanged: (value) {
                  controller.selectedKPIDivision = value;
                  controller.update();
                  controller.getAllKategoriKPI();
                },
                hint: Text(
                  "Select Category KPI",
                  style: TextStyle(
                      fontSize: 16.0,
                      color: AppColor.blackColor,
                      fontWeight: FontWeight.w500),
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(10)),
              ListView.builder(
                  itemCount: controller.listKategoriKPI!.length,
                  physics: NeverScrollableScrollPhysics(),
                  padding: EdgeInsets.all(ScreenUtil().setHeight(10)),
                  shrinkWrap: true,
                  itemBuilder: (context, index) => Padding(
                        padding: index == 0
                            ? EdgeInsets.all(0)
                            : EdgeInsets.only(top: ScreenUtil().setHeight(10)),
                        child: Container(
                          width: double.maxFinite,
                          padding: EdgeInsets.all(ScreenUtil().setHeight(10)),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(
                                  ScreenUtil().setHeight(10)),
                              boxShadow: [
                                BoxShadow(
                                    offset: Offset(0, 0),
                                    spreadRadius: 2,
                                    blurRadius: 1,
                                    color: AppColor.secondaryColor
                                        .withOpacity(0.5))
                              ]),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                child: Row(
                                  children: [
                                    InkWell(
                                        onTap: () {
                                          controller.allowAccess(controller
                                                      .selectedKPIDivision!
                                                      .id!) ==
                                                  true
                                              ? Get.dialog(EditKPIDialog(
                                                  kategoriKpiModel: controller
                                                      .listKategoriKPI![index],
                                                  controller: controller))
                                              : Get.dialog(RestrictionDialog());
                                        },
                                        child: Icon(
                                          Icons.edit,
                                          color: AppColor.blackColor,
                                        )),
                                    SizedBox(width: ScreenUtil().setHeight(10)),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          controller.listKategoriKPI![index]
                                              .kategoriKpi,
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              color: AppColor.blackColor,
                                              fontSize: 15,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        Text(
                                          "Target: ${controller.listKategoriKPI![index].target}",
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              color: AppColor.blackColor,
                                              fontSize: 13,
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  controller.allowAccess(controller
                                              .selectedKPIDivision!.id!) ==
                                          true
                                      ? Get.dialog(DeleteConfirmationDialog(
                                          title: 'KPI',
                                          subtitle: controller
                                              .listKategoriKPI![index]
                                              .kategoriKpi,
                                          deleteFunction: () {
                                            controller.deleteKategoriKPI(
                                                controller
                                                    .listKategoriKPI![index]
                                                    .id!);
                                            Get.back();
                                          }))
                                      : Get.dialog(RestrictionDialog());
                                },
                                child: Icon(
                                  Icons.delete,
                                  color: Colors.redAccent,
                                ),
                              )
                            ],
                          ),
                        ),
                      )),
            ],
          ),
        )),
      ),
    );
  }
}
