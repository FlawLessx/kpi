import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:kpi_system/screen/pages/admin/presentation/controller/admin_controller.dart';
import 'package:kpi_system/screen/pages/profile/presentation/view/profile_view.dart';
import 'package:kpi_system/screen/utils/app_color.dart';

import 'admin_divisi_widget.dart';
import 'admin_konfirmasi_widget.dart';
import 'admin_kpi_widget.dart';
import 'widget/admin_drawer.dart';

class AdminView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AdminController>(
      init: AdminController(),
      builder: (controller) => Scaffold(
          drawer: AdminDrawer(
            adminController: controller,
          ),
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: Builder(
                builder: (context) => IconButton(
                    icon: Icon(
                      AntDesign.appstore1,
                      color: AppColor.secondaryColor,
                    ),
                    onPressed: () {
                      Scaffold.of(context).openDrawer();
                    })),
          ),
          body: IndexedStack(
            index: controller.selectedIndex,
            children: [
              AdminDivisiWidget(adminController: controller),
              AdminKPIWidget(adminController: controller),
              AdminKonfirmasiWidget(adminController: controller),
              ProfileView()
            ],
          ),
          bottomNavigationBar: Container(
            decoration: BoxDecoration(color: Colors.white, boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.6),
                  offset: Offset(0, -1),
                  spreadRadius: 2,
                  blurRadius: 2)
            ]),
            child: Padding(
              padding: EdgeInsets.all(ScreenUtil().setHeight(10)),
              child: GNav(
                  backgroundColor: Colors.white,
                  onTabChange: (index) {
                    controller.selectedIndex = index;
                    controller.update();
                  },
                  haptic: true,
                  tabBorderRadius: ScreenUtil().setHeight(25),
                  curve: Curves.easeIn,
                  color: Colors.grey,
                  gap: ScreenUtil().setHeight(5),
                  activeColor: Colors.white,
                  iconSize: ScreenUtil().setHeight(25),
                  padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setHeight(20),
                      vertical: ScreenUtil().setHeight(10)),
                  tabBackgroundColor: AppColor.blackColor,
                  tabs: [
                    GButton(
                      icon: Icons.info_outline,
                      text: 'Divisi',
                    ),
                    GButton(
                      icon: Icons.list_alt_outlined,
                      text: 'KPI',
                    ),
                    GButton(
                      icon: Icons.done_outline_rounded,
                      text: 'Konfirmasi',
                    ),
                  ]),
            ),
          )),
    );
  }
}
