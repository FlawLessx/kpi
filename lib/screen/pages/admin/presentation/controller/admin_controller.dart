import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:permission_handler/permission_handler.dart';
import 'package:path_provider/path_provider.dart' as path;
import 'package:open_file/open_file.dart';

import '../../../../../core/failures/failures.dart';
import '../../../../../core/models/divisi_model.dart';
import '../../../../../core/models/kategori_kpi_model.dart';
import '../../../../../core/models/nilai_kpi_model.dart';
import '../../../../../core/models/user_model.dart';
import '../../../../shared/domain/drawer_model.dart';
import '../../../../utils/app_pages.dart';
import '../../data/admin_repository.dart';
import '../../domain/entity/detail_kpi_model.dart';
import '../view/widget/detail_kpi_bottomsheet.dart';
import '../view/widget/restriction_dialog.dart';

class AdminController extends GetxController {
  late final TextEditingController addDivisionController =
      TextEditingController();
  late final TextEditingController kpiNamaController = TextEditingController();
  late final TextEditingController kpiTargetController =
      TextEditingController();
  AdminRepository _adminRepository = AdminRepository();
  List<DivisiModel>? listDivisi = [];
  List<KategoriKpiModel>? listKategoriKPI = [];
  DivisiModel? selectedDivision;
  DivisiModel? selectedKPIDivision;
  bool divisionSelected = true;
  int selectedIndex = 0;
  GlobalKey<FormState> divisiFormKey = GlobalKey<FormState>();
  GlobalKey<FormState> kpiFormKey = GlobalKey<FormState>();
  int selectedDrawerIndex = 0;
  UserModel? userModel;
  List<DrawerModel> listDrawerItem = [];
  List<NilaiKpiModel> listNilaiKpi = [];
  List<KategoriKpiModel> listKpi = [];
  List rowsTable = [];
  List<int> listNilai = [];
  List<String> listKategoriKPIName = [];
  NilaiKpiModel? detailNilaiKPIModel;
  List<Widget> listBody = [];
  String? userDivision;

  @override
  void onInit() {
    getAllDivisi();
    _body();
    _getDataKategori();
    super.onInit();
  }

  _body() {}

  _addDrawerItem() async {
    listDrawerItem = [
      DrawerModel(
          title: "Home",
          icons: Icons.home,
          function: () {
            selectedIndex = 0;
            update();
          }),
    ];

    if (userModel!.email != "admin@kpi.com") {
      listDrawerItem.add(DrawerModel(
          title: "Profile",
          icons: Icons.account_box_outlined,
          function: () {
            selectedIndex = 3;
            update();
          }));
    }
    update();
  }

  _getUser() async {
    userModel = await _adminRepository.getCurrentUser();
    listDivisi?.forEach((element) {
      if (element.id == userModel!.kodeDivisi) {
        userDivision = element.namaDivisi;
      }
    });
    _addDrawerItem();
    update();
  }

  //
  // DIVISION FUNCTION
  //
  getAllDivisi() async {
    try {
      if (listDivisi!.isNotEmpty) listDivisi!.clear();

      listDivisi = await _adminRepository.getAllDivisi();
      _getUser();
      update();
    } on Failures catch (e) {
      Get.showSnackbar(GetBar(
        title: "Error",
        message: e.toString(),
      ));
    }
  }

  deleteDivision(int divisionId) async {
    try {
      await _adminRepository.deleteDivisi(divisionId);
      getAllDivisi();
    } on Failures catch (e) {
      Get.showSnackbar(GetBar(
        title: "Error",
        message: e.toString(),
      ));
    }
  }

  createDivision() async {
    if (divisiFormKey.currentState!.validate()) {
      try {
        await _adminRepository
            .insertDivisi(DivisiModel(namaDivisi: addDivisionController.text));
        addDivisionController.text = '';
        update();

        Fluttertoast.showToast(
            msg: "Divisi Berhasil Ditambahkan",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.black,
            textColor: Colors.white,
            fontSize: 15.0);
        getAllDivisi();
      } on Failures catch (e) {
        Get.showSnackbar(GetBar(
          title: "Error",
          message: e.toString(),
        ));
      }
    }
  }

  //
  // KATEGORI KPI FUNCTION
  //
  getAllKategoriKPI({int? divisonId}) async {
    try {
      if (listKategoriKPI!.isNotEmpty) listKategoriKPI!.clear();

      listKategoriKPI = await _adminRepository.getKategoriKPIByDivision(
          divisonId == null ? selectedKPIDivision!.id! : divisonId);
      update();
    } on Failures catch (e) {
      Get.showSnackbar(GetBar(
        title: "Error",
        message: e.toString(),
      ));
    }
  }

  deleteKategoriKPI(int kategoriID) async {
    try {
      if (allowAccess(userModel!.kodeDivisi) == true) {
        await _adminRepository.deleteKPI(kategoriID);
        getAllKategoriKPI();
      } else {
        Get.dialog(RestrictionDialog());
      }
    } on Failures catch (e) {
      Get.showSnackbar(GetBar(
        title: "Error",
        message: e.toString(),
      ));
    }
  }

  updateKPI(KategoriKpiModel kategoriKpiModel) async {
    try {
      if (allowAccess(userModel!.kodeDivisi) == true) {
        await _adminRepository.updateKategoriKPI(kategoriKpiModel);
        getAllKategoriKPI();
      } else {
        Get.dialog(RestrictionDialog());
      }
    } on Failures catch (e) {
      Get.showSnackbar(GetBar(
        title: "Error",
        message: e.toString(),
      ));
    }
  }

  createKategoriKPI() async {
    if (kpiFormKey.currentState!.validate()) {
      if (selectedDivision != null) {
        if (allowAccess(userModel!.kodeDivisi) == true) {
          try {
            await _adminRepository.insertKategoriKPI(KategoriKpiModel(
                kodeDivisi: selectedDivision!.id!,
                kategoriKpi: kpiNamaController.text,
                target: int.parse(kpiTargetController.text)));

            Fluttertoast.showToast(
                msg: "KPI Berhasil Ditambahkan",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.black,
                textColor: Colors.white,
                fontSize: 15.0);

            getAllKategoriKPI(divisonId: selectedDivision!.id);

            kpiNamaController.text = '';
            kpiTargetController.text = '';
            selectedDivision = null;
            divisionSelected = true;
            update();
          } on Failures catch (e) {
            Get.showSnackbar(GetBar(
              title: "Error",
              message: e.toString(),
            ));
          }
        } else {
          Get.dialog(RestrictionDialog());
        }
      } else {
        divisionSelected = false;
        update();
      }
    }
  }

  bool allowAccess(int divisonId) {
    if (divisonId == userModel?.kodeDivisi ||
        userModel!.email == "admin@kpi.com")
      return true;
    else
      return false;
  }

  //
  // CONFIRMATION FUNCTION
  //

  _getDataKategori() async {
    try {
      listNilai.clear();
      listKpi.clear();
      rowsTable.clear();

      if (userModel == null) {
        userModel = await _adminRepository.getCurrentUser();
      }

      if (userModel!.email == "admin@kpi.com") {
        listNilaiKpi = (await _adminRepository.getAllNilaiKPI())!;
      } else {
        listNilaiKpi = (await _adminRepository
            .getNilaiKPIByDivision(userModel!.kodeDivisi))!;
      }

      listKpi = (await _adminRepository
          .getKategoriKPIByDivision(userModel!.kodeDivisi))!;

      if (listNilaiKpi.isNotEmpty) {
        for (var item in listNilaiKpi) {
          rowsTable.add(
            {
              "ID": "id=" + item.id.toString(),
              "Tanggal": item.tanggal,
              "Status": item.status,
              "Bukti Pendukung": "bukti.pdfbukti=" + item.bukti,
              "Aksi": item.status == 'Disetujui'
                  ? "lock"
                  : "editid=${item.id.toString()}",
            },
          );
        }
      } else {
        rowsTable.add(
          {
            "Tanggal": "",
            "Status": "",
            "Bukti Pendukung": "",
            "Aksi": "",
          },
        );
      }

      update();
    } on Failures catch (e) {
      Get.showSnackbar(GetBar(
        title: "Error",
        message: e.toString(),
      ));
    }
  }

  getNilaiKPIById(int id) async {
    try {
      listNilai.clear();
      listKategoriKPIName.clear();
      detailNilaiKPIModel = null;

      for (var i = 0; i < listNilaiKpi.length; i++) {
        if (listNilaiKpi[i].id == id) {
          for (var j = 0; j < listNilaiKpi[i].idKategory.length; j++) {
            listNilai.add(listNilaiKpi[i].skorKpi[j]);
          }

          detailNilaiKPIModel = listNilaiKpi[i];
          break;
        }
      }

      for (var item in listKpi) {
        for (var temp in detailNilaiKPIModel!.idKategory) {
          if (temp == item.id) {
            listKategoriKPIName.add(item.kategoriKpi);
          }
        }
      }
      update();

      await _adminRepository.updateNilaiKPI(NilaiKpiModel(
          id: detailNilaiKPIModel!.id,
          idKategory: detailNilaiKPIModel!.idKategory,
          kodeDivisi: detailNilaiKPIModel!.kodeDivisi,
          skorKpi: detailNilaiKPIModel!.skorKpi,
          idKaryawan: detailNilaiKPIModel!.idKaryawan,
          bukti: detailNilaiKPIModel!.bukti,
          status: detailNilaiKPIModel!.status,
          lock: detailNilaiKPIModel!.lock == false
              ? true
              : detailNilaiKPIModel!.lock,
          tanggal: detailNilaiKPIModel!.tanggal, alasan: detailNilaiKPIModel!.alasan));

      final data =
          await _adminRepository.getUserByID(detailNilaiKPIModel!.idKaryawan);

      String? namaDivisi;

      if (userModel!.email != "admin@kpi.com") {
        listDivisi?.forEach((element) {
          if (element.id == data!.kodeDivisi) {
            namaDivisi = element.namaDivisi;
          }
        });
      } else {
        namaDivisi = "admin";
      }

      Get.bottomSheet(
          DetailKPIBottomSheet(
            detailKPIModel: DetailKPIModel(
                id: detailNilaiKPIModel!.id!,
                namaDivisi: namaDivisi!,
                kpiName: listKategoriKPIName,
                kpiSkor: listNilai,
                image: detailNilaiKPIModel!.bukti,
                tanggal: detailNilaiKPIModel!.tanggal,
                nama: data!.nama,
                status: detailNilaiKPIModel!.status,
                alasan: detailNilaiKPIModel!.alasan != null
                    ? detailNilaiKPIModel!.alasan!
                    : ""),
            adminController: this,
          ),
          isDismissible: true,
          isScrollControlled: true,
          ignoreSafeArea: false);
    } on Failures catch (e) {
      Get.showSnackbar(GetBar(
        title: "Error",
        message: e.toString(),
      ));
    }
  }

  updateStatusKPI(bool accept, {String? alasan = ''}) async {
    try {
      Get.back();

      final data = await _adminRepository.updateNilaiKPI(NilaiKpiModel(
          id: detailNilaiKPIModel!.id,
          idKategory: detailNilaiKPIModel!.idKategory,
          kodeDivisi: detailNilaiKPIModel!.kodeDivisi,
          skorKpi: detailNilaiKPIModel!.skorKpi,
          idKaryawan: detailNilaiKPIModel!.idKaryawan,
          bukti: detailNilaiKPIModel!.bukti,
          status: accept == true ? "Disetujui" : "Ditolak",
          lock: alasan != null ? false : detailNilaiKPIModel!.lock,
          tanggal: detailNilaiKPIModel!.tanggal,
          alasan: alasan));

      _getDataKategori();

      if (data != null) {
        Fluttertoast.showToast(
            msg: "Status Berhasil Dirubah",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.black,
            textColor: Colors.white,
            fontSize: 15.0);
      }
    } on Failures catch (e) {
      Get.showSnackbar(GetBar(
        title: "Error",
        message: e.toString(),
      ));
    }
  }

  deleteKPI(int id) async {
    try {
      Get.back();
      await _adminRepository.deleteNilaiKPI(id);

      Fluttertoast.showToast(
          msg: "Berhasil Menghapus KPI",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.black,
          textColor: Colors.white,
          fontSize: 15.0);

      _getDataKategori();
    } on Failures catch (e) {
      Get.showSnackbar(GetBar(
        title: "Error",
        message: e.toString(),
      ));
    }
  }

  saveBuktiToPDF(String imagePath) async {
    final pdf = pw.Document();
    final image = pw.MemoryImage(File(imagePath).readAsBytesSync());

    pdf.addPage(pw.Page(build: (pw.Context context) {
      return pw.Center(
        child: pw.Image(image),
      );
    }));

    var status = await Permission.storage.status;
    if (!status.isGranted) {
      await Permission.storage.request();
    }

    if (status.isGranted) {
      final filePath = await path.getExternalStorageDirectory();

      final file = File("${filePath!.path}/bukti.pdf");
      await file.writeAsBytes(await pdf.save());
      OpenFile.open(file.path);
    }
  }

  logout() {
    Get.offAllNamed(Routes.LOGIN);
  }
}
