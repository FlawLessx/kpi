import 'dart:convert';

import 'package:sqflite/sqflite.dart';

import '../../../../core/datasources/database/db_helper.dart';
import '../../../../core/datasources/pref/pref_manager.dart';
import '../../../../core/failures/failures.dart';
import '../../../../core/models/divisi_model.dart';
import '../../../../core/models/user_model.dart';
import '../../../../service_locator.dart';

abstract class IProfileRepository {
  Future<UserModel?> getCurrentUser();
  Future<int?> updateUser(UserModel user);
  Future<DivisiModel?> getDivisi(int divisionId);
  Future<UserModel?> getUserByEmail(String email);
  Future<UserModel?> getUserByID(int id);
  Future<void> insertCurrentUser(String encode);
}

class ProfileRepository implements IProfileRepository {
  late final DBHelper _dbHelper = getIt.get<DBHelper>();
  final PrefsManager _prefsManager = getIt.get<PrefsManager>();

  @override
  Future<UserModel?> getCurrentUser() async {
    try {
      final data = await _prefsManager.getString('user');
      return UserModel.fromJson(jsonDecode(data!));
    } on DatabaseException {
      throw Failures("Failed Get Current User");
    }
  }

  @override
  Future<int?> updateUser(UserModel user) async {
    try {
      return await _dbHelper.updateUser(user);
    } on DatabaseException {
      throw Failures("Failed Update User");
    }
  }

  @override
  Future<DivisiModel?> getDivisi(int divisionId) async {
    try {
      return await _dbHelper.getDivisi(divisionId);
    } on DatabaseException {
      throw Failures("Failed Get Division");
    }
  }

  @override
  Future<UserModel?> getUserByEmail(String email) async {
    try {
      return await _dbHelper.getUserByEmail(email);
    } on DatabaseException {
      throw Failures("Failed Get User");
    }
  }

  @override
  Future<UserModel?> getUserByID(int id) async {
    try {
      return await _dbHelper.getUserByID(id);
    } on DatabaseException {
      throw Failures("Failed Get User");
    }
  }

  @override
  Future<void> insertCurrentUser(String encode) async {
    try {
      await _prefsManager.setString('user', encode);
    } on DatabaseException {
      throw Failures("Failed Save Current User");
    }
  }
}
