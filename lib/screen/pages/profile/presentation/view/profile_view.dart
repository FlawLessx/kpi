import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:get/utils.dart';

import '../../../../shared/widget/custom_button.dart';
import '../../../../shared/widget/custom_textfield_2.dart';
import '../../../../utils/app_color.dart';
import '../controller/profile_controller.dart';

class ProfileView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProfileController>(
        init: ProfileController(),
        builder: (controller) => Scaffold(
            appBar: null,
            body: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.only(
                    left: ScreenUtil().setHeight(20),
                    right: ScreenUtil().setHeight(20),
                    bottom: ScreenUtil().setHeight(20)),
                child: Form(
                  key: controller.formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Profile",
                        style: TextStyle(
                            color: AppColor.blackColor,
                            fontWeight: FontWeight.w800,
                            fontSize: 28),
                      ),
                      CustomTextfield2(
                        labelText: "Full Name",
                        controller: controller.nameController,
                        errorMessage: "",
                        useOutlineBorder: false,
                        validator: (value) {
                          if (value == "" || value == null) {
                            return "Full name is required";
                          }

                          return null;
                        },
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(20),
                      ),
                      Text(
                        "Gender",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: AppColor.blackColor),
                      ),
                      Container(
                        height: ScreenUtil().setHeight(30),
                        child: Row(
                          children: [
                            Radio<int>(
                              value: controller.selectedGender,
                              groupValue: 0,
                              onChanged: (value) {
                                controller.selectedGender = 0;
                                controller.update();
                              },
                            ),
                            Text(
                              'Male',
                              style: new TextStyle(
                                  fontSize: 16.0,
                                  color: AppColor.blackColor,
                                  fontWeight: FontWeight.w500),
                            ),
                            Radio<int>(
                              value: controller.selectedGender,
                              groupValue: 1,
                              onChanged: (value) {
                                controller.selectedGender = 1;
                                controller.update();
                              },
                            ),
                            Text(
                              'Female',
                              style: TextStyle(
                                  fontSize: 16.0,
                                  color: AppColor.blackColor,
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                      CustomTextfield2(
                        labelText: "Email",
                        controller: controller.emailController,
                        errorMessage: "",
                        useOutlineBorder: false,
                        validator: (value) {
                          if (value == "" || value == null) {
                            return "Email is required";
                          }

                          if (value != "" && GetUtils.isEmail(value) == false) {
                            return "Please provide a valid e-mail";
                          }

                          if (value == "admin@kpi.com") {
                            return "E-mail already used";
                          }

                          return null;
                        },
                      ),
                      Visibility(
                        visible: controller.isEmailUsed,
                        child: Text(
                          "E-mail already used",
                          style: TextStyle(
                              color: Colors.red.shade700, fontSize: 12),
                        ),
                      ),
                      CustomTextfield2(
                        labelText: "Username",
                        controller: controller.usernameController,
                        errorMessage: "",
                        useOutlineBorder: false,
                        validator: (value) {
                          if (value == "" || value == null) {
                            return "Username is required";
                          }

                          return null;
                        },
                      ),
                      CustomTextfield2(
                        labelText: "Phone Number",
                        controller: controller.noTelpController,
                        errorMessage: "",
                        useOnlyNumber: true,
                        useOutlineBorder: false,
                        validator: (value) {
                          if (value == "" || value == null) {
                            return "Phone number is required";
                          }

                          if (value != "" &&
                              GetUtils.isPhoneNumber(value) == false) {
                            return "Please provide a valid phone number";
                          }

                          return null;
                        },
                      ),
                      Container(
                        width: ScreenUtil().setWidth(150),
                        child: CustomTextfield2(
                          labelText: "Password",
                          controller: controller.passwordController,
                          errorMessage: "",
                          showHidePassword: true,
                          useOutlineBorder: false,
                          validator: (value) {
                            if (value == "" || value == null) {
                              return "Password is required";
                            }

                            if (value.length < 8) {
                              return "Minimum 8 characters";
                            }

                            return null;
                          },
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(10),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Roles: ${controller.roles}",
                            style: TextStyle(
                                fontSize: 16.0,
                                color: AppColor.blackColor,
                                fontWeight: FontWeight.w500),
                          ),
                          Text(
                            "Division: ${controller.division}",
                            style: TextStyle(
                                fontSize: 16.0,
                                color: AppColor.blackColor,
                                fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(20),
                      ),
                      CustomButton(
                          text: "Update",
                          onTap: () {
                            controller.updateUser();
                          }),
                      SizedBox(
                        height: ScreenUtil().setHeight(20),
                      ),
                    ],
                  ),
                ),
              ),
            )));
  }
}
