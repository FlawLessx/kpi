import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';

import '../../../../../core/failures/failures.dart';
import '../../../../../core/models/user_model.dart';
import '../../data/profile_repository.dart';

class ProfileController extends GetxController {
  late final TextEditingController nameController = TextEditingController();
  late final TextEditingController emailController = TextEditingController();
  late final TextEditingController passwordController = TextEditingController();
  late final TextEditingController noTelpController = TextEditingController();
  late final TextEditingController usernameController = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  String? division;
  String? roles;
  ProfileRepository _profileRepository = ProfileRepository();
  UserModel? userModel;
  bool isEmailUsed = false;
  int selectedGender = 0;

  @override
  void onInit() {
    getUser();
    super.onInit();
  }

  getUser() async {
    try {
      if (userModel == null) {
        userModel = await _profileRepository.getCurrentUser();
      }

      if (userModel!.email != "admin@kpi.com") {
        nameController.text = userModel!.nama;
        emailController.text = userModel!.email;
        passwordController.text = userModel!.password;
        noTelpController.text = userModel!.noTelp;
        usernameController.text = userModel!.userName;
        roles = userModel!.rolesId == 2 ? "Employee" : "Manager";
        final data = await _profileRepository.getDivisi(userModel!.kodeDivisi);
        division = data!.namaDivisi;
      }

      update();
    } on Failures catch (e) {
      Get.showSnackbar(GetBar(
        title: "Error",
        message: e.toString(),
      ));
    }
  }

  Future<UserModel?>? _checkEmailInUsed() async {
    return await _profileRepository.getUserByEmail(emailController.text);
  }

  updateUser() async {
    if (formKey.currentState!.validate()) {
      final data = await _checkEmailInUsed();

      if (data == null || data.email == userModel!.email) {
        isEmailUsed = false;
        update();
        try {
          final model = UserModel(
              id: userModel!.id,
              nama: nameController.text,
              kodeDivisi: userModel!.kodeDivisi,
              email: emailController.text,
              password: passwordController.text,
              noTelp: noTelpController.text,
              userName: usernameController.text,
              rolesId: userModel!.rolesId,
              jenisKelamin: selectedGender == 0 ? true : false);

          await _profileRepository.updateUser(model);

          Get.showSnackbar(GetBar(
            title: "Success",
            message: "Account Updated",
          ));

          await _profileRepository
              .insertCurrentUser(jsonEncode(model.toJson()));
          userModel = null;
          getUser();
        } on Failures catch (e) {
          Get.showSnackbar(GetBar(
            title: "Error",
            message: e.toString(),
          ));
        }
      } else {
        isEmailUsed = true;
        update();
      }
    }
  }
}
