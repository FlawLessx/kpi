import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/state_manager.dart';
import 'package:get/utils.dart';

import '../../../../../../core/models/divisi_model.dart';
import '../../../../../../core/models/roles_model.dart';
import '../../../../../shared/widget/custom_button.dart';
import '../../../../../shared/widget/custom_textfield_2.dart';
import '../../../../../utils/app_color.dart';
import '../../controller/login_controller.dart';

class RegisterWidget extends StatelessWidget {
  final LoginController loginController;

  RegisterWidget({required this.loginController});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoginController>(
        init: loginController,
        builder: (controller) => Container(
              child: Form(
                key: controller.formRegisterKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Create a new \naccount",
                      style: TextStyle(
                          color: AppColor.blackColor,
                          fontWeight: FontWeight.w800,
                          fontSize: 28),
                    ),
                    CustomTextfield2(
                      labelText: "Full Name",
                      controller: controller.regisNamaController,
                      errorMessage: "",
                      useOutlineBorder: false,
                      validator: (value) {
                        if (value == "" || value == null) {
                          return "Full name is required";
                        }

                        return null;
                      },
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(20),
                    ),
                    Text(
                      "Gender",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: AppColor.blackColor),
                    ),
                    Container(
                      height: ScreenUtil().setHeight(30),
                      child: Row(
                        children: [
                          Radio<int>(
                            value: controller.selectedGender,
                            groupValue: 0,
                            onChanged: (value) {
                              controller.selectedGender = 0;
                              controller.update();
                            },
                          ),
                          Text(
                            'Male',
                            style: new TextStyle(
                                fontSize: 16.0,
                                color: AppColor.blackColor,
                                fontWeight: FontWeight.w500),
                          ),
                          Radio<int>(
                            value: controller.selectedGender,
                            groupValue: 1,
                            onChanged: (value) {
                              controller.selectedGender = 1;
                              controller.update();
                            },
                          ),
                          Text(
                            'Female',
                            style: TextStyle(
                                fontSize: 16.0,
                                color: AppColor.blackColor,
                                fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                    CustomTextfield2(
                      labelText: "Email",
                      controller: controller.regisEmailController,
                      errorMessage: "",
                      useOutlineBorder: false,
                      validator: (value) {
                        if (value == "" || value == null) {
                          return "Email is required";
                        }

                        if (value != "" && GetUtils.isEmail(value) == false) {
                          return "Please provide a valid e-mail";
                        }

                        if (value == "admin@kpi.com") {
                          return "E-mail already used";
                        }

                        return null;
                      },
                    ),
                    Visibility(
                      visible: controller.isEmailUsed,
                      child: Text(
                        "E-mail already used",
                        style:
                            TextStyle(color: Colors.red.shade700, fontSize: 12),
                      ),
                    ),
                    CustomTextfield2(
                      labelText: "Username",
                      controller: controller.regisuserNameController,
                      errorMessage: "",
                      useOutlineBorder: false,
                      validator: (value) {
                        if (value == "" || value == null) {
                          return "Username is required";
                        }

                        return null;
                      },
                    ),
                    CustomTextfield2(
                      labelText: "Phone Number",
                      controller: controller.regisNoTelpController,
                      errorMessage: "",
                      useOnlyNumber: true,
                      useOutlineBorder: false,
                      validator: (value) {
                        if (value == "" || value == null) {
                          return "Phone number is required";
                        }

                        if (value != "" &&
                            GetUtils.isPhoneNumber(value) == false) {
                          return "Please provide a valid phone number";
                        }

                        return null;
                      },
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: ScreenUtil().setWidth(150),
                          child: CustomTextfield2(
                            labelText: "Password",
                            controller: controller.regisPasswordController,
                            errorMessage: "",
                            showHidePassword: true,
                            useOutlineBorder: false,
                            validator: (value) {
                              if (value == "" || value == null) {
                                return "Password is required";
                              }

                              if (value.length < 8) {
                                return "Minimum 8 characters";
                              }

                              return null;
                            },
                          ),
                        ),
                        Container(
                          width: ScreenUtil().setWidth(150),
                          child: CustomTextfield2(
                            labelText: "Confirm Pass",
                            showHidePassword: true,
                            controller:
                                controller.regisConfirmPasswordController,
                            errorMessage: "",
                            useOutlineBorder: false,
                            validator: (value) {
                              if (value == "" || value == null) {
                                return "Enter pass again";
                              }

                              if (value !=
                                  controller.regisPasswordController.text) {
                                return "Must be same";
                              }

                              return null;
                            },
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(10),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            DropdownButton<RolesModel>(
                              value: controller.selectedRoles,
                              items: controller.listRoles!
                                  .map((e) => DropdownMenuItem(
                                        child: Text(
                                          e.namaRole,
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              color: AppColor.blackColor,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        value: e,
                                      ))
                                  .toList(),
                              onChanged: (value) {
                                controller.selectedRoles = value;
                                controller.update();
                              },
                              hint: Text(
                                "Select Roles",
                                style: TextStyle(
                                    fontSize: 16.0,
                                    color: AppColor.blackColor,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                            Visibility(
                              visible: !controller.isRoleSelected,
                              child: Text(
                                "Roles required",
                                style: TextStyle(
                                    color: Colors.red.shade700, fontSize: 12),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            DropdownButton<DivisiModel>(
                              value: controller.selectedDivision,
                              items: controller.listDivision!
                                  .map((e) => DropdownMenuItem(
                                        child: Text(
                                          e.namaDivisi,
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              color: AppColor.blackColor,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        value: e,
                                      ))
                                  .toList(),
                              onChanged: (value) {
                                controller.selectedDivision = value;
                                controller.update();
                              },
                              hint: Text(
                                "Select Division",
                                style: TextStyle(
                                    fontSize: 16.0,
                                    color: AppColor.blackColor,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                            Visibility(
                              visible: !controller.isDivisionSelected,
                              child: Text(
                                "Division required",
                                style: TextStyle(
                                    color: Colors.red.shade700, fontSize: 12),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(20),
                    ),
                    CustomButton(
                        text: "Register",
                        onTap: () {
                          controller.registerUser();
                        }),
                    SizedBox(
                      height: ScreenUtil().setHeight(20),
                    ),
                  ],
                ),
              ),
            ));
  }
}
