import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../../shared/widget/custom_button.dart';
import '../../../../../shared/widget/custom_textfield.dart';
import '../../../../../utils/app_color.dart';
import '../../controller/login_controller.dart';

class LoginWidget extends StatelessWidget {
  final LoginController loginController;

  LoginWidget({required this.loginController});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoginController>(
        init: loginController,
        builder: (controller) => Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Welcome to",
                  style: TextStyle(
                      color: AppColor.blackColor,
                      fontWeight: FontWeight.w800,
                      fontSize: 28),
                ),
                Text(
                  "KPI System",
                  style: TextStyle(
                      color: AppColor.mainColor,
                      fontWeight: FontWeight.w800,
                      fontSize: 28),
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(10),
                ),
                Image.asset(
                  "src/img/Logo.PNG",
                  filterQuality: FilterQuality.high,
                  height: ScreenUtil().setHeight(80),
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(10),
                ),
                CustomTextfield(
                    title: "Email",
                    controller: controller.loginEmailController,
                    icons: Icons.email_outlined,
                    hintText: "Insert Email *"),
                SizedBox(
                  height: ScreenUtil().setHeight(10),
                ),
                CustomTextfield(
                    title: "Password",
                    controller: controller.loginPassController,
                    isPassword: true,
                    icons: Icons.lock_outline,
                    hintText: "Insert Password *"),
                SizedBox(
                  height: ScreenUtil().setHeight(5),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Visibility(
                      visible: controller.accountNotFound,
                      child: Text(
                        "Account not found",
                        style: TextStyle(
                          color: Colors.red.shade700,
                          fontSize: 15,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(20),
                ),
                CustomButton(
                    text: "Login",
                    onTap: () {
                      controller.login();
                    })
              ],
            ));
  }
}
