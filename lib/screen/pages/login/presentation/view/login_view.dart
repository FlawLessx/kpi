import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../shared/widget/slide_switch.dart';
import '../../../../utils/app_color.dart';
import '../controller/login_controller.dart';
import 'widget/login_widget.dart';
import 'widget/register_widget.dart';

class LoginView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoginController>(
      init: LoginController(),
      builder: (controller) => Scaffold(
        resizeToAvoidBottomInset: controller.isSignUp,
        extendBodyBehindAppBar: true,
        appBar: null,
        body: SafeArea(
          child: LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              return SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                      minWidth: constraints.maxWidth,
                      minHeight: constraints.maxHeight),
                  child: IntrinsicHeight(
                    child: Padding(
                      padding: EdgeInsets.all(ScreenUtil().setHeight(20)),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                              child: controller.isSignUp == false
                                  ? LoginWidget(loginController: controller)
                                  : RegisterWidget(
                                      loginController: controller)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SlidingSwitch(
                                value: false,
                                width: ScreenUtil().setWidth(200),
                                onChanged: (value) {},
                                height: ScreenUtil().setHeight(40),
                                animationDuration:
                                    const Duration(milliseconds: 400),
                                onTap: () {
                                  controller.isSignUp = !controller.isSignUp;
                                  controller.update();
                                },
                                onSwipe: () {
                                  controller.isSignUp = !controller.isSignUp;
                                  controller.update();
                                },
                                textOff: "Sign In",
                                textOn: "Sign Up",
                                colorOn: Colors.white,
                                colorOff: Colors.white,
                                background: Colors.white,
                                buttonColor: AppColor.blackColor,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
