import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../../core/datasources/pref/pref_manager.dart';
import '../../../../../core/failures/failures.dart';
import '../../../../../core/models/divisi_model.dart';
import '../../../../../core/models/roles_model.dart';
import '../../../../../core/models/user_model.dart';
import '../../data/login_repository.dart';
import '../../../../utils/app_pages.dart';
import '../../../../../service_locator.dart';

class LoginController extends GetxController {
  late final LoginRepository _loginRepository = LoginRepository();

  bool isSignUp = false;
  int selectedGender = 0;
  late final TextEditingController loginEmailController =
      TextEditingController();
  late final TextEditingController loginPassController =
      TextEditingController();
  late final TextEditingController regisEmailController =
      TextEditingController();
  late final TextEditingController regisNamaController =
      TextEditingController();
  late final TextEditingController regisuserNameController =
      TextEditingController();
  late final TextEditingController regisNoTelpController =
      TextEditingController();
  late final TextEditingController regisPasswordController =
      TextEditingController();
  late final TextEditingController regisConfirmPasswordController =
      TextEditingController();
  final PrefsManager prefsManager = getIt.get<PrefsManager>();

  List<DivisiModel>? listDivision = [];
  List<RolesModel>? listRoles = [];
  DivisiModel? selectedDivision;
  RolesModel? selectedRoles;
  GlobalKey<FormState> formLoginKey = GlobalKey<FormState>();
  GlobalKey<FormState> formRegisterKey = GlobalKey<FormState>();
  bool isRoleSelected = true;
  bool isDivisionSelected = true;
  bool accountNotFound = false;
  bool isEmailUsed = false;

  @override
  void onInit() {
    _getAllDivision();
    _getAllRoles();
    super.onInit();
  }

  _getAllDivision() async {
    try {
      listDivision = await _loginRepository.getAllDivisi();
      update();
    } on Failures catch (e) {
      Get.showSnackbar(GetBar(
        title: "Error",
        message: e.toString(),
      ));
    }
  }

  _getAllRoles() async {
    try {
      listRoles = await _loginRepository.getAllRole();

      if (listDivision!.isEmpty) {
        await _createRolesData();
        listRoles = await _loginRepository.getAllRole();
      }
      update();
    } on Failures catch (e) {
      Get.showSnackbar(GetBar(
        title: "Error",
        message: e.toString(),
      ));
    }
  }

  _createRolesData() async {
    List<String> tempListRoles = [
      "Manager",
      "Employee",
    ];

    tempListRoles.forEach((element) async {
      await _loginRepository.insertRole(RolesModel(namaRole: element));
    });
  }

  login() async {
    try {
      if (loginEmailController.text == "admin@kpi.com" &&
          loginPassController.text == "adminkpi") {
        Get.showSnackbar(GetBar(
          title: "Success",
          message: "Processing",
          showProgressIndicator: true,
        ));

        await prefsManager.setString(
            "user",
            jsonEncode(UserModel(
                    nama: "Admin",
                    kodeDivisi: 0,
                    email: "admin@kpi.com",
                    password: "adminkpi",
                    noTelp: "0000000",
                    rolesId: 0,
                    jenisKelamin: true,
                    userName: "admin_kpi")
                .toJson()));

        await Future.delayed(Duration(seconds: 3), () {
          Get.offAllNamed(Routes.ADMIN);
        });
      } else {
        final tempUser = await _loginRepository.getUser(
            loginEmailController.text, loginPassController.text);

        if (tempUser != null) {
          accountNotFound = false;
          update();
          Get.showSnackbar(GetBar(
            title: "Success",
            message: "Processing",
            showProgressIndicator: true,
          ));

          final roles = await _loginRepository.getRole(tempUser.rolesId);

          await prefsManager.setString("user", jsonEncode(tempUser.toJson()));

          await Future.delayed(Duration(seconds: 3), () {
            Get.offAllNamed(
                roles!.namaRole == "Employee" ? Routes.MAIN : Routes.ADMIN);
          });
        } else {
          accountNotFound = true;
          update();
        }
      }
    } on Failures catch (e) {
      Get.showSnackbar(GetBar(
        title: "Error",
        message: e.toString(),
      ));
    }
  }

  Future<UserModel?>? _checkEmailInUsed() async {
    return await _loginRepository.getUserByEmail(regisEmailController.text);
  }

  registerUser() async {
    if (formRegisterKey.currentState!.validate()) {
      final data = await _checkEmailInUsed();

      if (data == null) {
        isEmailUsed = false;
        update();
        try {
          await _loginRepository.insertUser(UserModel(
              nama: regisNamaController.text,
              kodeDivisi: selectedDivision!.id!,
              email: regisEmailController.text,
              password: regisPasswordController.text,
              noTelp: regisNoTelpController.text,
              userName: regisuserNameController.text,
              rolesId: selectedRoles!.id!,
              jenisKelamin: selectedGender == 0 ? true : false));

          regisNamaController.text = '';
          selectedDivision = null;
          regisEmailController.text = '';
          regisPasswordController.text = '';
          regisNoTelpController.text = '';
          regisConfirmPasswordController.text = '';
          regisuserNameController.text = '';
          selectedRoles = null;
          selectedGender = 0;
          update();

          Get.showSnackbar(GetBar(
            title: "Success",
            message: "Account Created",
          ));
        } on Failures catch (e) {
          Get.showSnackbar(GetBar(
            title: "Error",
            message: e.toString(),
          ));
        }
      } else {
        isEmailUsed = true;
        update();
      }
    } else {
      if (selectedRoles == null) {
        isRoleSelected = false;
      } else {
        isRoleSelected = true;
      }

      if (selectedDivision == null) {
        isDivisionSelected = false;
      } else {
        isDivisionSelected = true;
      }
      update();
    }
  }
}
