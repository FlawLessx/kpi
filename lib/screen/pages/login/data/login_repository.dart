import 'package:sqflite/sqflite.dart';

import '../../../../core/datasources/database/db_helper.dart';
import '../../../../core/failures/failures.dart';
import '../../../../core/models/divisi_model.dart';
import '../../../../core/models/roles_model.dart';
import '../../../../core/models/user_model.dart';
import '../../../../service_locator.dart';

abstract class ILoginRepository {
  Future<UserModel?> insertUser(UserModel user);
  Future<UserModel?> getUser(String email, String password);
  Future<RolesModel?> insertRole(RolesModel role);
  Future<List<RolesModel>?> getAllRole();
  Future<List<DivisiModel>?> getAllDivisi();
  Future<DivisiModel?> insertDivisi(DivisiModel division);
  Future<UserModel?> getUserByEmail(String email);
  Future<RolesModel?> getRole(int roleId);
}

class LoginRepository implements ILoginRepository {
  late final DBHelper _dbHelper = getIt.get<DBHelper>();

  @override
  Future<List<DivisiModel>?> getAllDivisi() async {
    try {
      return await _dbHelper.getAllDivisi();
    } on DatabaseException {
      throw Failures("Failed Get All Division Data");
    }
  }

  @override
  Future<List<RolesModel>?> getAllRole() async {
    try {
      return await _dbHelper.getAllRole();
    } on DatabaseException {
      throw Failures("Failed Get All Roles Data");
    }
  }

  @override
  Future<UserModel?> getUser(String email, String password) async {
    try {
      return await _dbHelper.getUser(email, password);
    } on DatabaseException {
      throw Failures("Failed Get User");
    }
  }

  @override
  Future<RolesModel?> insertRole(RolesModel role) async {
    try {
      return await _dbHelper.insertRole(role);
    } on DatabaseException {
      throw Failures("Failed Create Roles");
    }
  }

  @override
  Future<UserModel?> insertUser(UserModel user) async {
    try {
      return await _dbHelper.insertUser(user);
    } on DatabaseException {
      throw Failures("Failed Create User");
    }
  }

  @override
  Future<DivisiModel?> insertDivisi(DivisiModel division) async {
    try {
      return await _dbHelper.insertDivisi(division);
    } on DatabaseException {
      throw Failures("Failed Create Division");
    }
  }

  @override
  Future<UserModel?> getUserByEmail(String email) async {
    try {
      return await _dbHelper.getUserByEmail(email);
    } on DatabaseException {
      throw Failures("Failed Get User");
    }
  }

  @override
  Future<RolesModel?> getRole(int roleId) async {
    try {
      return await _dbHelper.getRole(roleId);
    } on DatabaseException {
      throw Failures("Failed Get User");
    }
  }
}
