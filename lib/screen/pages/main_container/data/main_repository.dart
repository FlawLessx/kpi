import 'dart:convert';

import 'package:kpi_system/core/datasources/pref/pref_manager.dart';
import 'package:sqflite/sqflite.dart';

import '../../../../core/datasources/database/db_helper.dart';
import '../../../../core/failures/failures.dart';
import '../../../../core/models/divisi_model.dart';
import '../../../../core/models/user_model.dart';
import '../../../../service_locator.dart';

abstract class IMainContainerRepository {
  Future<UserModel?> getCurrentUser();
  Future<List<DivisiModel>?> getAllDivisi();
}

class MainContainerRepository implements IMainContainerRepository {
  late final DBHelper _dbHelper = getIt.get<DBHelper>();
  final PrefsManager _prefsManager = getIt.get<PrefsManager>();

  @override
  Future<List<DivisiModel>?> getAllDivisi() async {
    try {
      return await _dbHelper.getAllDivisi();
    } on DatabaseException {
      throw Failures("Failed Get All Division Data");
    }
  }

  @override
  Future<UserModel?> getCurrentUser() async {
    try {
      final data = await _prefsManager.getString('user');
      return UserModel.fromJson(jsonDecode(data!));
    } on DatabaseException {
      throw Failures("Failed Get Current User");
    }
  }
}
