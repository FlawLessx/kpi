import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:kpi_system/core/failures/failures.dart';
import 'package:kpi_system/screen/pages/main_container/data/main_repository.dart';

import '../../../../../core/models/user_model.dart';
import '../../../../shared/domain/drawer_model.dart';
import '../../../../utils/app_pages.dart';
import '../../../home/presentation/view/home_view.dart';
import '../../../input_kpi/presentation/view/input_kpi_view.dart';
import '../../../profile/presentation/view/profile_view.dart';

class MainContainerController extends GetxController {
  int selectedDrawerIndex = 0;
  int selectedBodyIndex = 0;
  UserModel? userModel;
  final MainContainerRepository _mainContainerRepository =
      MainContainerRepository();
  List<DrawerModel> listDrawerItem = [];
  String? userDivision;
  List<Widget> listBody = [HomeView(), ProfileView(), InputKPIView()];

  @override
  void onInit() {
    _addDrawerItem();
    _getUser();
    super.onInit();
  }

  _addDrawerItem() {
    listDrawerItem = [
      DrawerModel(
          title: "Home",
          icons: Icons.home,
          function: () {
            selectedBodyIndex = 0;
            update();
          }),
      DrawerModel(
          title: "Profile",
          icons: Icons.account_box_outlined,
          function: () {
            selectedBodyIndex = 1;
            update();
          }),
      DrawerModel(
          title: "Input KPI",
          icons: Icons.list_alt,
          function: () {
            selectedBodyIndex = 2;
            update();
          }),
    ];
    update();
  }

  _getUser() async {
    try {
      userModel = await _mainContainerRepository.getCurrentUser();
      final listDivisi = await _mainContainerRepository.getAllDivisi();

      listDivisi?.forEach((element) {
        if (element.id == userModel!.kodeDivisi) {
          userDivision = element.namaDivisi;
        }
      });

      update();
    } on Failures catch (e) {
      Get.showSnackbar(GetBar(
        title: "Error",
        message: e.toString(),
      ));
    }
  }

  logout() {
    Get.offAllNamed(Routes.LOGIN);
  }
}
