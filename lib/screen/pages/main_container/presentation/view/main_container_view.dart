import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:get/state_manager.dart';

import '../../../../utils/app_color.dart';
import '../controller/main_container_contoller.dart';
import 'widget/drawer.dart';

class MainContainerView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<MainContainerController>(
        init: MainContainerController(),
        builder: (controller) => Scaffold(
            drawer: DrawerWidget(
              mainContainerController: controller,
            ),
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              leading: Builder(
                  builder: (context) => IconButton(
                      icon: Icon(
                        AntDesign.appstore1,
                        color: AppColor.secondaryColor,
                      ),
                      onPressed: () {
                        Scaffold.of(context).openDrawer();
                      })),
            ),
            body: controller.listBody[controller.selectedBodyIndex]));
  }
}
