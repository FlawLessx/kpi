import 'dart:convert';

import 'package:sqflite/sqflite.dart';

import '../../../../core/datasources/database/db_helper.dart';
import '../../../../core/datasources/pref/pref_manager.dart';
import '../../../../core/failures/failures.dart';
import '../../../../core/models/kategori_kpi_model.dart';
import '../../../../core/models/nilai_kpi_model.dart';
import '../../../../core/models/user_model.dart';
import '../../../../service_locator.dart';

abstract class IInputKPIRepository {
  Future<UserModel> getCurrentUser();
  Future<List<KategoriKpiModel>?> getKategoriKPIByDivision(int divisionId);
  Future<List<NilaiKpiModel>?> getNilaiKPIByEmployeID(int employeId);
  Future<NilaiKpiModel?> insertNilaiKPI(NilaiKpiModel nilaiKpiModel);
  Future<int?> deleteAllNilaiKPI();
  Future<int?> updateNilaiKPI(NilaiKpiModel nilaiKpiModel);
}

class InputKPIRepository implements IInputKPIRepository {
  late final DBHelper _dbHelper = getIt.get<DBHelper>();
  late final PrefsManager _prefsManager = getIt.get<PrefsManager>();

  @override
  Future<List<KategoriKpiModel>?> getKategoriKPIByDivision(
      int divisionId) async {
    try {
      return await _dbHelper.getKategoriKPIByDivision(divisionId);
    } on DatabaseException {
      throw Failures("Failed Get KPI By Division");
    }
  }

  @override
  Future<UserModel> getCurrentUser() async {
    try {
      final data = await _prefsManager.getString('user');
      return UserModel.fromJson(jsonDecode(data!));
    } on DatabaseException {
      throw Failures("Failed Get Current User");
    }
  }

  @override
  Future<List<NilaiKpiModel>?> getNilaiKPIByEmployeID(int employeId) async {
    try {
      return await _dbHelper.getNilaiKPIByEmployeID(employeId);
    } on DatabaseException {
      throw Failures("Failed Get Nilai KPI By Employee");
    }
  }

  @override
  Future<NilaiKpiModel?> insertNilaiKPI(NilaiKpiModel nilaiKpiModel) async {
    try {
      return await _dbHelper.insertNilaiKPI(nilaiKpiModel);
    } on DatabaseException {
      throw Failures("Failed Get Nilai KPI By Employee");
    }
  }

  @override
  Future<int?> deleteAllNilaiKPI() async {
    try {
      return await _dbHelper.deleteAllNilaiKPI();
    } on DatabaseException {
      throw Failures("Failed Get Nilai KPI By Employee");
    }
  }

  @override
  Future<int?> updateNilaiKPI(NilaiKpiModel nilaiKpiModel) async {
    try {
      return await _dbHelper.updateNilaiKPI(nilaiKpiModel);
    } on DatabaseException {
      throw Failures("Failed Update KPI");
    }
  }
}
