import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:permission_handler/permission_handler.dart';
import 'package:path_provider/path_provider.dart' as path;
import 'package:open_file/open_file.dart';

import '../../../../../core/failures/failures.dart';
import '../../../../../core/models/kategori_kpi_model.dart';
import '../../../../../core/models/nilai_kpi_model.dart';
import '../../../../../core/models/user_model.dart';
import '../../data/input_kpi_repository.dart';
import '../view/widget/form_input_kpi.dart';

class InputKPIController extends GetxController {
  List<TextEditingController> listController = [];
  List<TextEditingController> editController = [];
  List<KategoriKpiModel> listKpi = [];
  List<NilaiKpiModel> listNilaiKpi = [];
  NilaiKpiModel? updateNilaiKPIModel;
  final InputKPIRepository _inputKPIRepository = InputKPIRepository();
  UserModel? userModel;
  String? buktiPath;
  List rowsTable = [];
  final _picker = ImagePicker();
  String alasan = '';

  @override
  void onInit() {
    _getDataKategori();
    super.onInit();
  }

  _getDataKategori() async {
    try {
      listController.clear();
      listKpi.clear();
      rowsTable.clear();

      if (userModel == null) {
        userModel = await _inputKPIRepository.getCurrentUser();
      }

      listKpi = (await _inputKPIRepository
          .getKategoriKPIByDivision(userModel!.kodeDivisi))!;

      if (listKpi.isNotEmpty) {
        listKpi.forEach((element) {
          listController.add(TextEditingController());
        });
      }

      listNilaiKpi =
          (await _inputKPIRepository.getNilaiKPIByEmployeID(userModel!.id!))!;

      if (listNilaiKpi.isNotEmpty) {
        for (var item in listNilaiKpi) {
          rowsTable.add(
            {
              "ID": "id=" + item.id.toString(),
              "Tanggal": item.tanggal,
              "Status": item.status,
              "Bukti Pendukung": "bukti.pdfbukti=" + item.bukti,
              "Aksi": item.status == "Menunggu" || item.status == "Ditolak"
                  ? "editid=${item.id.toString()}"
                  : "lock",
            },
          );
        }
      } else {
        rowsTable.add(
          {
            "Tanggal": "",
            "Status": "",
            "Bukti Pendukung": "",
            "Aksi": "",
          },
        );
      }

      update();
    } on Failures catch (e) {
      Get.showSnackbar(GetBar(
        title: "Error",
        message: e.toString(),
      ));
    }
  }

  insertNilaiKpi({bool isUpdate = false}) async {
    try {
      Get.back();
      Intl.defaultLocale = 'id_ID';
      String dateNow = DateFormat('dd-MM-yyyy').format(DateTime.now());

      if (listKpi.isNotEmpty) {
        List<int> listKpiId = [];
        List<int> listSkor = [];

        for (var i = 0; i < listKpi.length; i++) {
          listKpiId.add(listKpi[i].id!);
          listSkor.add(int.parse(isUpdate == false
              ? listController[i].text
              : editController[i].text));
        }

        if (isUpdate == false) {
          await _inputKPIRepository.insertNilaiKPI(NilaiKpiModel(
            idKategory: listKpiId,
            kodeDivisi: userModel!.kodeDivisi,
            skorKpi: listSkor,
            idKaryawan: userModel!.id!,
            bukti: buktiPath!,
            status: "Menunggu",
            lock: false,
            tanggal: dateNow,
          ));
        } else {
          await _inputKPIRepository.updateNilaiKPI(NilaiKpiModel(
              id: updateNilaiKPIModel!.id,
              kodeDivisi: updateNilaiKPIModel!.kodeDivisi,
              idKategory: listKpiId,
              skorKpi: listSkor,
              idKaryawan: updateNilaiKPIModel!.idKaryawan,
              bukti: buktiPath!,
              status: updateNilaiKPIModel!.status == "Ditolak"
                  ? "Menunggu"
                  : updateNilaiKPIModel!.status,
              lock: updateNilaiKPIModel!.lock,
              tanggal: updateNilaiKPIModel!.tanggal,
              alasan: updateNilaiKPIModel!.alasan));
        }

        for (var i = 0; i < listKpi.length; i++) {
          listController[i].text = '';
        }
        buktiPath = null;
        update();
      }

      _getDataKategori();

      Fluttertoast.showToast(
          msg: isUpdate == false
              ? "Nilai KPI Berhasil Ditambahkan"
              : "Berhasil Diupdate",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.black,
          textColor: Colors.white,
          fontSize: 15.0);
    } on Failures catch (e) {
      Get.showSnackbar(GetBar(
        title: "Error",
        message: e.toString(),
      ));
    }
  }

  Future<String?> addImage() async {
    final pickedFile = await _picker.getImage(source: ImageSource.gallery);
    return pickedFile!.path;
  }

  getNilaiKPIById(int id) {
    try {
      editController.clear();
      String? imagePath;

      for (var i = 0; i < listNilaiKpi.length; i++) {
        if (listNilaiKpi[i].id == id) {
          for (var j = 0; j < listNilaiKpi[i].idKategory.length; j++) {
            editController.add(TextEditingController(
                text: listNilaiKpi[i].skorKpi[j].toString()));
          }

          imagePath = listNilaiKpi[i].bukti;
          updateNilaiKPIModel = listNilaiKpi[i];
          print(
              "Alasan: ${listNilaiKpi[i].alasan != null ? listNilaiKpi[i].alasan : ""}");
          alasan = listNilaiKpi[i].alasan ?? "";
          break;
        }
      }
      update();

      Get.dialog(FormInputKPI(
        controller: this,
        listTextController: editController,
        isEdit: true,
        imagePath: imagePath,
      ));
    } on Failures catch (e) {
      Get.showSnackbar(GetBar(
        title: "Error",
        message: e.toString(),
      ));
    }
  }

  saveBuktiToPDF(String imagePath) async {
    final pdf = pw.Document();
    final image = pw.MemoryImage(File(imagePath).readAsBytesSync());

    pdf.addPage(pw.Page(build: (pw.Context context) {
      return pw.Center(
        child: pw.Image(image),
      );
    }));

    var status = await Permission.storage.status;
    if (!status.isGranted) {
      await Permission.storage.request();
    }

    if (status.isGranted) {
      final filePath = await path.getExternalStorageDirectory();

      final file = File("${filePath!.path}/bukti.pdf");
      await file.writeAsBytes(await pdf.save());
      OpenFile.open(file.path);
    }
  }

  int calculateBottomRange(int target) {
    return (target * 0.4).round();
  }

  deleteDB() async {
    await _inputKPIRepository.deleteAllNilaiKPI();
  }
}
