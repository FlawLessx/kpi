import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../../shared/widget/custom_button_2.dart';
import '../../../../../shared/widget/custom_textfield_2.dart';
import '../../../../../utils/app_color.dart';
import '../../../../admin/presentation/view/widget/container_item.dart';
import '../../controller/input_kpi_controller.dart';

class FormInputKPI extends StatefulWidget {
  final InputKPIController controller;
  final bool isEdit;
  final List<TextEditingController>? listTextController;
  final String? imagePath;

  FormInputKPI(
      {required this.controller,
      this.isEdit = false,
      this.listTextController,
      this.imagePath});

  @override
  _FormInputKPIState createState() => _FormInputKPIState();
}

class _FormInputKPIState extends State<FormInputKPI> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool isBuktilNull = false;
  String? image;

  Future takeImage() async {
    image = await widget.controller.addImage();
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    if (widget.imagePath != null) {
      image = widget.imagePath;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        widget.isEdit == true ? "Edit KPI" : "Masukkan KPI",
        style: TextStyle(
            color: AppColor.blackColor,
            fontWeight: FontWeight.bold,
            fontSize: 18),
      ),
      content: SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: widget.controller.listKpi.length,
                  itemBuilder: (context, index) => CustomTextfield2(
                        labelText: widget
                                .controller.listKpi[index].kategoriKpi +
                            " (${widget.controller.calculateBottomRange(widget.controller.listKpi[index].target)}-${widget.controller.listKpi[index].target})",
                        controller: widget.listTextController == null
                            ? widget.controller.listController[index]
                            : widget.listTextController![index],
                        validator: (value) {
                          if (value == null || value == '') {
                            return "Required fields";
                          }

                          if (int.parse(value) >
                                  widget.controller.listKpi[index].target ||
                              int.parse(value) <
                                  widget.controller.calculateBottomRange(widget
                                      .controller.listKpi[index].target)) {
                            return "Not valid";
                          }
                          return null;
                        },
                      )),
              SizedBox(height: ScreenUtil().setHeight(20)),
              containerItem(
                  Container(
                    height: ScreenUtil().setHeight(150),
                    width: double.infinity,
                    child: GestureDetector(
                      onTap: () async {
                        takeImage();
                      },
                      child: image != null
                          ? Image.file(
                              File(image!),
                              width: double.infinity,
                              height: ScreenUtil().setHeight(130),
                              fit: BoxFit.cover,
                            )
                          : Center(
                              child: Icon(
                                Icons.upload_file,
                                color: AppColor.mainColor,
                                size: ScreenUtil().setHeight(50),
                              ),
                            ),
                    ),
                  ),
                  padding: 10),
              SizedBox(height: ScreenUtil().setHeight(5)),
              Visibility(
                visible: isBuktilNull,
                child: Text(
                  "Masukkan bukti",
                  style: TextStyle(
                    color: Colors.red.shade700,
                    fontSize: 15,
                  ),
                ),
              ),
              Visibility(
                visible: widget.controller.alasan != '' ? true : false,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Alasan Penolakan: ",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: AppColor.blackColor)),
                    Text(widget.controller.alasan,
                        style: TextStyle(
                            fontSize: 13, color: AppColor.blackColor)),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      actions: [
        TextButton(
            onPressed: () {
              Get.back();
            },
            child: Text(
              'Batal',
              style: TextStyle(color: Colors.red),
            )),
        CustomButton2(
            title: widget.isEdit == true ? "Edit KPI" : "Ajukan KPI",
            onTap: () {
              if (formKey.currentState!.validate()) {
                if (image == null) {
                  isBuktilNull = true;
                  setState(() {});
                } else {
                  isBuktilNull = false;
                  setState(() {});

                  widget.controller.buktiPath = image;
                  if (widget.isEdit == false) {
                    widget.controller.insertNilaiKpi();
                  } else {
                    widget.controller.insertNilaiKpi(isUpdate: true);
                  }
                }
              }
            })
      ],
    );
  }
}
