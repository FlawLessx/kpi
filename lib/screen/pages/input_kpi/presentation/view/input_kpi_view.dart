import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:json_table/json_table.dart';

import '../../../../utils/app_color.dart';
import '../controller/input_kpi_controller.dart';
import 'widget/form_input_kpi.dart';

class InputKPIView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<InputKPIController>(
      init: InputKPIController(),
      builder: (controller) => Scaffold(
        floatingActionButton: FloatingActionButton.extended(
            onPressed: () {
              Get.dialog(FormInputKPI(
                controller: controller,
              ));
            },
            label: Row(
              children: [
                Icon(Icons.add),
                Text("Masukkan KPI"),
              ],
            )),
        appBar: null,
        body: ListView(
          padding: EdgeInsets.only(
              left: ScreenUtil().setHeight(20),
              right: ScreenUtil().setHeight(20),
              bottom: ScreenUtil().setHeight(20)),
          children: [
            Text(
              "Riwayat KPI",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: AppColor.blackColor,
              ),
            ),
            SizedBox(height: ScreenUtil().setHeight(10)),
            if (controller.rowsTable.isNotEmpty)
              JsonTable(
                controller.rowsTable,
                showColumnToggle: false,
                paginationRowCount: 10,
                tableHeaderBuilder: (String? header) {
                  return Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                    decoration: BoxDecoration(
                        border:
                            Border.all(width: 0.5, color: AppColor.blackColor),
                        color: AppColor.mainColor),
                    child: Text(
                      header!,
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white),
                    ),
                  );
                },
                tableCellBuilder: (value) {
                  return Container(
                    height: ScreenUtil().setHeight(50),
                    padding:
                        EdgeInsets.symmetric(horizontal: 4.0, vertical: 2.0),
                    decoration: BoxDecoration(
                        border:
                            Border.all(width: 0.5, color: AppColor.blackColor)),
                    child: Center(
                      child: value.toString().toLowerCase().contains('bukti=')
                          ? InkWell(
                              onTap: () {
                                controller.saveBuktiToPDF(
                                    value.toString().split('bukti=')[1]);
                              },
                              child: Text(
                                value.toString().split('bukti=')[0],
                                maxLines: 4,
                                textAlign: TextAlign.center,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    color: Colors.blue,
                                    decoration: TextDecoration.underline),
                              ),
                            )
                          : value.toString().toLowerCase().contains('edit')
                              ? InkWell(
                                  onTap: () {
                                    controller.getNilaiKPIById(int.parse(
                                        value.toString().split('id=')[1]));
                                  },
                                  child: Icon(Icons.edit,
                                      color: AppColor.mainColor))
                              : value.toString().toLowerCase().contains('lock')
                                  ? Icon(Icons.lock, color: Colors.red)
                                  : value
                                          .toString()
                                          .toLowerCase()
                                          .contains('id=')
                                      ? Text(
                                          value.toString().split('id=')[1],
                                          maxLines: 4,
                                          textAlign: TextAlign.center,
                                          overflow: TextOverflow.ellipsis,
                                        )
                                      : Text(
                                          value.toString(),
                                          maxLines: 4,
                                          textAlign: TextAlign.center,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                    ),
                  );
                },
                onRowSelect: (index, value) {},
              )
          ],
        ),
      ),
    );
  }
}
