part of 'app_pages.dart';

class Routes {
  static const String LOGIN = '/login';
  static const String HOME = '/home';
  static const String ADMIN = '/admin';
  static const String INPUT_KPI = '/input_kpi';
  static const String MAIN = '/main';
}
