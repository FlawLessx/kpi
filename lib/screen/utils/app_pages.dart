import 'package:get/get.dart';
import 'package:kpi_system/screen/pages/admin/presentation/view/admin_view.dart';
import 'package:kpi_system/screen/pages/home/presentation/view/home_view.dart';
import 'package:kpi_system/screen/pages/input_kpi/presentation/view/input_kpi_view.dart';
import 'package:kpi_system/screen/pages/login/presentation/view/login_view.dart';
import 'package:kpi_system/screen/pages/main_container/presentation/view/main_container_view.dart';

part 'app_routes.dart';

class AppPages {
  static const INITIAL = Routes.LOGIN;

  static final routes = [
    GetPage(name: Routes.LOGIN, page: () => LoginView()),
    GetPage(name: Routes.HOME, page: () => HomeView()),
    GetPage(name: Routes.ADMIN, page: () => AdminView()),
    GetPage(name: Routes.INPUT_KPI, page: () => InputKPIView()),
    GetPage(name: Routes.MAIN, page: () => MainContainerView())
  ];
}
