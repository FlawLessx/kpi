import 'package:flutter/material.dart';

class AppColor {
  AppColor._();

  static const Color mainColor = Color(0xFF58968b);
  static const Color secondaryColor = Color(0xFFd1d6da);
  static const Color blackColor = Color(0xFF3d4144);
  static const Color blackSecondaryColor = Color(0xFF464c4b);
  static const Color switchSecondaryColor = Color(0xFFe4e4e4);
  static const Color darkRedColor = Color(0xFF9c6874);
  static const Color darkBlueColor = Color(0xFF96a9c9);
  static const Color progressBlueColor = Color(0xFF8bb2ff);
  static const Color progressRedColor = Color(0xFFf398a1);
  static const Color backgroundBlueColor = Color(0xFFf2f7fa);
  static const Color statusPendingColor = Color(0xFF2F80ED);
  static const Color statusConfirmColor = Color(0xFF27AE60);
  static const Color statusRejectColor = Color(0xFFEB5757);
  static const Color textfieldColor = Color(0xFFf9f9f9);
  static const Color yellowColor = Color(0xFFd1a54a);
  static const Color greyColor = Color(0xFFa2a8b2);
  static const Color grapeColor = Color(0xFF7a5be4);
}
