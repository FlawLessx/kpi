import 'package:flutter/widgets.dart';

class DrawerModel {
  final String title;
  final IconData icons;
  final Function function;

  DrawerModel(
      {required this.title, required this.icons, required this.function});
}
