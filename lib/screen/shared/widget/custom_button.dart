import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kpi_system/screen/utils/app_color.dart';

class CustomButton extends StatelessWidget {
  final String? text;
  final Function()? onTap;
  final bool useOutline;
  final bool useIcon;
  final IconData icon;
  final Color color;

  CustomButton(
      {@required this.text,
      @required this.onTap,
      this.useOutline = false,
      this.useIcon = false,
      this.icon = Icons.qr_code_scanner_outlined,
      this.color = AppColor.mainColor});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: ScreenUtil().screenWidth,
        height: ScreenUtil().setHeight(50),
        decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(ScreenUtil().setHeight(10))),
        child: Center(
          child: Text(
            text!,
            style: TextStyle(
                color: Colors.white, fontSize: 17, fontWeight: FontWeight.w600),
          ),
        ),
      ),
    );
  }
}
