import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../utils/app_color.dart';
import '../../utils/app_font.dart';

class CustomTextfield2 extends StatefulWidget {
  final TextEditingController controller;
  final FocusNode? focusNode;
  final bool showHidePassword;
  final bool isPassword;
  final String? hintText;
  final Icon? prefixIcon;
  final Widget? suffixIcon;
  final String? labelText;
  final String? errorMessage;
  final bool? isEmail;
  final String? initialValue;
  final bool useOutlineBorder;
  final bool isConfirmPassword;
  final String? Function(String?)? validator;
  final bool isName;
  final bool useOnlyNumber;
  final Function(String)? onChanged;

  CustomTextfield2(
      {required this.controller,
      this.focusNode,
      this.isPassword = false,
      this.showHidePassword = false,
      this.hintText,
      this.prefixIcon,
      this.suffixIcon,
      this.labelText,
      this.errorMessage,
      this.isEmail,
      this.initialValue,
      this.useOutlineBorder = false,
      this.isConfirmPassword = false,
      this.validator,
      this.isName = false,
      this.useOnlyNumber = false,
      this.onChanged});

  @override
  _CustomTextfield2State createState() => _CustomTextfield2State();
}

class _CustomTextfield2State extends State<CustomTextfield2> {
  bool isFocus = false;

  _focusHandler(bool focus) {
    if (focus == true || widget.controller.text != '') {
      isFocus = true;
    } else {
      isFocus = false;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FocusScope(
      child: Focus(
        onFocusChange: (focus) {
          _focusHandler(focus);
        },
        child: TextFormField(
          cursorColor: AppColor.mainColor,
          controller: widget.controller,
          obscureText: widget.showHidePassword,
          keyboardType: widget.useOnlyNumber == false
              ? TextInputType.text
              : TextInputType.number,
          style: TextStyle(
              fontWeight: FontWeight.w500,
              fontFamily: AppFont.montserrat,
              color: AppColor.blackColor,
              fontSize: 16),
          textCapitalization: widget.isName == true
              ? TextCapitalization.words
              : TextCapitalization.none,
          decoration: new InputDecoration(
              border: widget.useOutlineBorder == true
                  ? OutlineInputBorder(
                      borderRadius:
                          BorderRadius.circular(ScreenUtil().setHeight(10)),
                      borderSide: BorderSide(
                        color: AppColor.mainColor,
                        width: 2,
                      ))
                  : UnderlineInputBorder(
                      borderSide: BorderSide(color: AppColor.mainColor)),
              focusedBorder: widget.useOutlineBorder == true
                  ? OutlineInputBorder(
                      borderRadius:
                          BorderRadius.circular(ScreenUtil().setHeight(10)),
                      borderSide: BorderSide(
                        color: AppColor.mainColor,
                        width: 2,
                      ))
                  : UnderlineInputBorder(
                      borderSide: BorderSide(color: AppColor.mainColor)),
              enabledBorder: widget.useOutlineBorder == true
                  ? OutlineInputBorder(
                      borderRadius:
                          BorderRadius.circular(ScreenUtil().setHeight(10)),
                      borderSide: BorderSide(
                          color: Colors.grey.withOpacity(0.6), width: 2))
                  : UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey)),
              errorBorder: widget.useOutlineBorder == true
                  ? OutlineInputBorder(
                      borderRadius:
                          BorderRadius.circular(ScreenUtil().setHeight(10)),
                      borderSide:
                          BorderSide(color: Colors.red.shade700, width: 2))
                  : UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.red.shade700)),
              hintStyle: TextStyle(fontWeight: FontWeight.w500),
              prefixIcon: widget.prefixIcon ?? widget.prefixIcon,
              suffixIcon: widget.suffixIcon ?? widget.suffixIcon,
              hintText: widget.hintText ?? widget.hintText,
              labelText: widget.labelText ?? widget.labelText,
              labelStyle: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontFamily: AppFont.montserrat,
                  color: isFocus == true
                      ? AppColor.mainColor
                      : AppColor.blackColor,
                  fontSize: 16)),
          initialValue: widget.initialValue ?? widget.initialValue,
          onChanged: widget.onChanged,
          validator: widget.validator,
        ),
      ),
    );
  }
}
