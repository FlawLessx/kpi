import 'package:flutter/material.dart';
import 'package:kpi_system/screen/utils/app_color.dart';

class CustomButton2 extends StatelessWidget {
  final String title;
  final Function() onTap;
  final Color color;

  CustomButton2(
      {required this.title, required this.onTap, this.color = Colors.black});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
              return color == Colors.black
                  ? AppColor.mainColor
                  : color; // Use the component's default.
            },
          ),
        ),
        onPressed: onTap,
        child: Center(
          child: Text(title),
        ));
  }
}
