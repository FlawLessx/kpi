import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kpi_system/screen/utils/app_color.dart';

class CustomTextfield extends StatefulWidget {
  final String title;
  final TextEditingController? controller;
  final bool onlyInputNumber;
  final bool isName;
  final IconData? icons;
  final String hintText;
  final bool isReadOnly;
  final Function()? onTap;
  final bool isOptional;
  final bool isPassword;
  CustomTextfield(
      {required this.title,
      required this.controller,
      this.onlyInputNumber = false,
      this.isName = false,
      this.icons,
      required this.hintText,
      this.isReadOnly = false,
      this.onTap,
      this.isPassword = false,
      this.isOptional = false});

  @override
  _CustomTextfieldState createState() => _CustomTextfieldState();
}

class _CustomTextfieldState extends State<CustomTextfield> {
  bool disabled = true;

  @override
  void initState() {
    super.initState();
  }

  _focusHandler(bool focus) {
    if (focus == true || widget.controller!.text != '') {
      disabled = false;
    } else {
      disabled = true;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(65),
      width: ScreenUtil().screenWidth,
      decoration: BoxDecoration(
          color:
              disabled == true ? AppColor.textfieldColor : Colors.transparent,
          borderRadius: BorderRadius.circular(ScreenUtil().setHeight(10)),
          border: Border.all(
              color: disabled == true
                  ? Colors.grey.withOpacity(0.3)
                  : AppColor.mainColor,
              width: 2)),
      child: FocusScope(
        child: Focus(
          onFocusChange: (focus) {
            _focusHandler(focus);
          },
          child: Padding(
            padding: EdgeInsets.only(
              left: ScreenUtil().setHeight(15),
            ),
            child: Center(
              child: Container(
                child: Padding(
                  padding: EdgeInsets.only(top: ScreenUtil().setHeight(4)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Visibility(
                          visible: !disabled,
                          child: RichText(
                              text: TextSpan(children: [
                            TextSpan(
                                text: '${widget.title} ',
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 14,
                                    color: Colors.black)),
                            TextSpan(
                                text: widget.isOptional == false ? '\*' : '',
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 14,
                                    color: Colors.red)),
                          ]))),
                      Container(
                        height: ScreenUtil().setHeight(30),
                        child: TextFormField(
                          maxLines: 1,
                          readOnly: widget.isReadOnly,
                          onTap: widget.onTap,
                          cursorColor: AppColor.secondaryColor,
                          controller: widget.controller,
                          obscureText: widget.isPassword,
                          textCapitalization: widget.isName == false
                              ? TextCapitalization.none
                              : TextCapitalization.words,
                          style: TextStyle(
                              color: AppColor.blackColor,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                          keyboardType: widget.onlyInputNumber == true
                              ? TextInputType.number
                              : TextInputType.text,
                          decoration: InputDecoration(
                              hintText:
                                  disabled == false ? '' : widget.hintText,
                              hintStyle: TextStyle(
                                  color: Colors.grey.withOpacity(0.5),
                                  fontSize: 17,
                                  fontWeight: FontWeight.w500),
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              contentPadding: EdgeInsets.only(
                                  bottom: ScreenUtil().setWidth(15)),
                              suffixIcon: Visibility(
                                visible: widget.icons != null ? true : false,
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      bottom: ScreenUtil().setHeight(5),
                                      right: 0),
                                  child: Icon(
                                    widget.icons,
                                    color: disabled == true
                                        ? Colors.grey.withOpacity(0.5)
                                        : AppColor.mainColor,
                                  ),
                                ),
                              )),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
