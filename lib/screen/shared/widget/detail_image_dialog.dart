import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../utils/app_color.dart';
import 'custom_button_2.dart';

class DetailImageDialog extends StatelessWidget {
  final String path;

  DetailImageDialog({required this.path});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        "Detail Bukti",
        style: TextStyle(
            color: AppColor.blackColor,
            fontWeight: FontWeight.bold,
            fontSize: 18),
      ),
      content: Image.file(
        File(path),
        width: double.infinity,
        height: ScreenUtil().setHeight(150),
        fit: BoxFit.cover,
      ),
      actions: [
        CustomButton2(
            title: "Tutup",
            onTap: () {
              Get.back();
            })
      ],
    );
  }
}
