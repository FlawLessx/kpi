import 'package:get_it/get_it.dart';

import 'core/datasources/database/db_helper.dart';
import 'core/datasources/pref/pref_manager.dart';

final getIt = GetIt.instance;

Future<void> setup() async {
  getIt.registerLazySingleton<DBHelper>(() => DBHelper());
  getIt.registerLazySingleton<PrefsManager>(() => PrefsManager());
}
